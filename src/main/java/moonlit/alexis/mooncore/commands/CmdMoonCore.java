package moonlit.alexis.mooncore.commands;

import moonlit.alexis.mooncore.MoonCore;
import moonlit.alexis.mooncore.configuration.Config;
import moonlit.alexis.mooncore.configuration.Data;
import moonlit.alexis.mooncore.configuration.Lang;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.Collections;
import java.util.List;

public class CmdMoonCore implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1 && "reload".startsWith(args[0].toLowerCase()) && sender.hasPermission("mooncore.command.staff.wildcore")) {
            return Collections.singletonList("reload");
        }

        if (args.length == 1 && "version".startsWith(args[0].toLowerCase()) && sender.hasPermission("mooncore.command.staff.wildcore")) {
            return Collections.singletonList("version");
        }

        if (args.length == 1 && "locale".startsWith(args[0].toLowerCase()) && sender.hasPermission("mooncore.command.staff.wildcore")) {
            return Collections.singletonList("locale");
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("mooncore.command.staff.wildcore")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        MoonCore plugin = MoonCore.getInstance();
        String response = "&d" + plugin.getName() + " v" + plugin.getDescription().getVersion();

        if (args.length > 0 && args[0].equalsIgnoreCase("reload")) {
            Config.reload(plugin);
            Lang.reload(plugin);
            Data.getInstance().reload();
            MoonCore.getInstance().getBot().setChatChannel(MoonCore.getInstance().getBot().getClient().getTextChannelById(Config.CHAT_CHANNEL));
            MoonCore.getInstance().getBot().setConsoleChannel(MoonCore.getInstance().getBot().getClient().getTextChannelById(Config.CONSOLE_CHANNEL));
            MoonCore.getInstance().getBot().setDebugChannel(MoonCore.getInstance().getBot().getClient().getTextChannelById(Config.DEBUG_CHANNEL));
            MoonCore.getInstance().getBot().setBroadcastChannel(MoonCore.getInstance().getBot().getClient().getTextChannelById(Config.BROADCAST_CHANNEL));

            response += " reloaded.";
        }
        Lang.send(sender, response);
        return true;
    }
}
