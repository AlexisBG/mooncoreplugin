package moonlit.alexis.mooncore.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

public class CmdPlugins implements TabExecutor {

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player && sender.hasPermission("mooncore.command.admin.plugins")) {
            sender.spigot().sendMessage();
        } else {
            sender.sendMessage("Plugins: " + Arrays.toString(Bukkit.getPluginManager().getPlugins()));
        }
        return true;
    }
}