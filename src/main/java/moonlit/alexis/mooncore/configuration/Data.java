package moonlit.alexis.mooncore.configuration;

import moonlit.alexis.mooncore.Logger;
import moonlit.alexis.mooncore.MoonCore;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class Data extends YamlConfiguration {
    private static Data data;

    public static Data getInstance() {
        if (data == null) {
            data = new Data();
        }
        return data;
    }

    private final File file;
    private final File folder;
    private final File language_folder;
    private final Object saveLock = new Object();

    public Data() {
        super();
        file = new File(MoonCore.getInstance().getDataFolder(), "data.yml");
        if (!file.exists()) {
            try {
                //noinspection ResultOfMethodCallIgnored
                file.createNewFile();
            } catch (IOException ignore) {
            }
        }

        folder = new File(MoonCore.getInstance().getDataFolder(), "custom_maps");
        if (!folder.exists()) {
            //noinspection ResultOfMethodCallIgnored
            folder.mkdir();
            Logger.info("Created directory `custom_maps`");
        }

        language_folder = new File(MoonCore.getInstance().getDataFolder(), "locales");
        if (!language_folder.exists()) {
            //noinspection ResultOfMethodCallIgnored
            language_folder.mkdir();
            Logger.info("Created directory `locales`");
        }
        reload();
        }
        

    public void reload() {
        synchronized (saveLock) {
            try {
                load(file);
            } catch (Exception ignore) {
            }
        }
    }

    private void save() {
        synchronized (saveLock) {
            try {
                save(file);
            } catch (Exception ignore) {
            }
        }
    }
}
