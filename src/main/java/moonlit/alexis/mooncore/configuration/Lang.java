package moonlit.alexis.mooncore.configuration;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

import moonlit.alexis.mooncore.MoonCore;
import moonlit.alexis.mooncore.util.ColorTranslator;
import org.bukkit.entity.Player;

import com.google.common.io.ByteStreams;
import com.google.common.io.ByteArrayDataOutput;
import java.io.DataOutputStream;
import java.io.ByteArrayOutputStream;
import com.google.common.collect.Iterables;
import java.io.IOException;



public class Lang {
    public static String COMMAND_NO_PERMISSION;

    public static String DISCORD_CHAT_FORMAT;
    public static String SERVER_ONLINE;
    public static String SERVER_OFFLINE;

    public static String ADVANCEMENT_ICON_TASK;
    public static String ADVANCEMENT_ICON_GOAL;
    public static String ADVANCEMENT_ICON_CHALLENGE;
    public static String DISCORD_ADVANCEMENT_FORMAT;
    public static String MINECRAFT_ADVANCEMENT_FORMAT;

    public static String SERVER_RESTART_10_MINUTES;
    public static String SERVER_RESTART_5_MINUTES;
    public static String SERVER_RESTART_1_MINUTE;
    public static String SERVER_RESTART_30_SECONDS;
    public static String SERVER_RESTART_10_SECONDS;
    public static String SERVER_RESTART_5_SECONDS;
    public static String SERVER_RESTART_FINAL;

    public static String MINECRAFT_CHAT_FORMAT;

    public static String DISCORD_PLAYERJOIN_MESSAGE;
    public static String DISCORD_PLAYERLEAVE_MESSAGE;
    public static String DISCORD_PLAYERDEATH_MESSAGE;
    public static String BROADCAST_FORMAT;
    public static String MINECRAFT_PLAYERJOIN_MESSAGE;
    public static String MINECRAFT_PLAYERLEAVE_MESSAGE;
    public static String DISCORD_PLAYERDISPLAY;

    public static void reload(JavaPlugin plugin) {
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), "locales/" + langFile);
        if (!configFile.exists()) {
            plugin.saveResource("locales/" +langFile, false);
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);


        SERVER_RESTART_10_MINUTES =  config.getString("server-restart-10-minutes" , "&7The server will be restarting in &610 &cminutes!");
        SERVER_RESTART_5_MINUTES =  config.getString("server-restart-5-minutes","&7The server will be restarting in &65 &cminutes!");
        SERVER_RESTART_1_MINUTE =  config.getString("server-restart-1-minute","&7The server will be restarting in &61 &cminute!");
        SERVER_RESTART_30_SECONDS =  config.getString("server-restart-30-seconds","&7The server will be restarting in &630 &cseconds!");
        SERVER_RESTART_10_SECONDS =  config.getString("server-restart-10-seconds","&7The server will be restarting in &610 &cseconds!");
        SERVER_RESTART_5_SECONDS =  config.getString("server-restart-5-seconds","&7The server will be restarting in &c6 &cseconds!");
        SERVER_RESTART_FINAL =  config.getString("server-restart-final", "&7The server will now restart!");

        SERVER_ONLINE = config.getString("server-online","🟢 **Server is online!**");
        SERVER_OFFLINE = config.getString("server-offline","🔴 **Server is offline!**");

        DISCORD_CHAT_FORMAT = config.getString("discord-chat-format","&7[&3Discord&7] [{userPrefix}&7] {userColor}{userName}#{userDiscriminator}&f: &7{message}");
        ADVANCEMENT_ICON_TASK = config.getString("advancement.icon.task",":medal:");
        ADVANCEMENT_ICON_GOAL = config.getString("advancement.icon.goal",":trophy:");
        ADVANCEMENT_ICON_CHALLENGE = config.getString("advancement.icon.challenge",":military_medal:");
        DISCORD_ADVANCEMENT_FORMAT = config.getString("advancement.discord-format", "{icon} {playerName} has completed the {type}, **[**{title}**]**!```{title}:\n  {description}```");
        MINECRAFT_ADVANCEMENT_FORMAT = config.getString("advancement.minecraft-format","{playerColor}{playerName}&f made the advancement: &5{title}");
        BROADCAST_FORMAT = config.getString("broadcast-format","📣 {message}");

        COMMAND_NO_PERMISSION = config.getString("command-no-permission", "&7You do not have permission for that command!");
        MINECRAFT_CHAT_FORMAT = config.getString("minecraft-chat-format", "{playerPrefix}{playerName}&7: {message}");

        DISCORD_PLAYERJOIN_MESSAGE = config.getString("discord-playerjoin-message","📥 **{playerName}** just joined the game!");

        DISCORD_PLAYERLEAVE_MESSAGE = config.getString("discord-playerleave-message","📤 **{playerName}** just left the game!");

        DISCORD_PLAYERDEATH_MESSAGE = config.getString("discord-playerdeath-message","💀 {message}");

        MINECRAFT_PLAYERJOIN_MESSAGE = config.getString("minecraft-playerjoin-message","{playerColor}{playerName}&f just joined &l{serverName}");

        MINECRAFT_PLAYERLEAVE_MESSAGE = config.getString("minecraft-playerleave-message","{playerColor}{playerName}&f just left &l{serverName}");

        DISCORD_PLAYERDISPLAY = config.getString("discord-playerdisplay","[{playerParentGroup}] {playerName}");

    }

    /**
     * Sends a message to a recipient
     *
     * @param recipient Recipient of message
     * @param message   Message to send
     */
    public static void send(CommandSender recipient, String message) {
        if (recipient != null) {
            for (String part : colorize(message).split("\n")) {
                if (part != null && !part.isEmpty()) {
                    recipient.sendMessage(part);
                }
            }
        }
    }

    /**
     * Broadcast a message to server
     *
     * @param message Message to broadcast
     */
    public static void broadcast(String message) {
        for (String part : colorize(message).split("\n")) {
            if (part != null && !part.isEmpty()) {
                Bukkit.broadcastMessage(part);
            }
        }
    }

    /**
     * Send a message to every players of the server (without triggering broadcast event)
     *
     * @param message Message to send
     */
    public static void sendToAllPlayers(String message,boolean format, boolean prefix) {
        if (prefix) message = Config.SERVER_MINECRAFT_PREFIX+message;
        if (format) message = colorize(message);
        for (String part : message.split("\n")) {
            if (part != null && !part.isEmpty()) {
                for (Player player:Bukkit.getOnlinePlayers()){
                    player.sendMessage(part);
                }
            }
        }
    }

    /**
     * Send a message to every players of the server (without triggering broadcast event)
     *
     * @param message Message to send
     */
    public static void sendToAllPlayers(String message) {
        sendToAllPlayers(message,true,true);
    }

    public static void sendToAllProxyPlayers(String message){
        
        sendToAllProxyPlayers(message, null,true,true);
    }

    public static void sendToAllProxyPlayers(String message,Player player,boolean format,boolean prefix) {
        if (player==null) player = Iterables.getFirst(Bukkit.getOnlinePlayers(), null);
        if (player==null) return;
        if (prefix) message = Config.SERVER_MINECRAFT_PREFIX+message;
        if (format) message = colorize(message);
        for (String part : message.split("\n")) {
            if (part != null && !part.isEmpty()) {
                ByteArrayDataOutput out = ByteStreams.newDataOutput();
                out.writeUTF("Forward"); // So BungeeCord knows to forward it
                out.writeUTF("ALL");
                out.writeUTF("mooncore-chat"); // The channel name to check if this your data
        
                ByteArrayOutputStream msgbytes = new ByteArrayOutputStream();
                DataOutputStream msgout = new DataOutputStream(msgbytes);
        
                try {
                    msgout.writeUTF(part); // You can do anything you want with msgout
                    // msgout.writeShort(123);
                    } catch (IOException exception){
                    exception.printStackTrace();
                    }
                byte[] barray =  msgbytes.toByteArray();          
                out.writeShort(barray.length);
                out.write(barray);
        
                System.out.println("Sending msg");
                player.sendPluginMessage(MoonCore.getInstance(), "BungeeCord", out.toByteArray());
            }
        }    
    }

    /**
     * Colorize a String
     *
     * @param str String to colorize
     * @return Colorized String
     */
    public static String colorize(String str) {
        if (str == null) {
            return "";
        }
        str = ColorTranslator.translateAlternateColorCodes(str);
        if (ColorTranslator.removeFormats(str).isEmpty()) {
            return "";
        }
        return str;
    }
}
