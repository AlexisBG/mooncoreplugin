package moonlit.alexis.mooncore.configuration;
import moonlit.alexis.mooncore.MoonCore;
import org.bukkit.configuration.file.FileConfiguration;
import moonlit.alexis.mooncore.util.ColorTranslator;
import java.util.Map;

public class Config {
    public static String SERVER_NAME;
    public static String SERVER_MINECRAFT_PREFIX;
    public static String SERVER_DISCORD_PREFIX;
    public static boolean DEBUG_MODE;
    public static String LANGUAGE_FILE;
    public static boolean COLOR_LOGS;
    public static String BOT_TOKEN;
    public static String CHAT_CHANNEL;
    public static String CONSOLE_CHANNEL;
    public static String DEBUG_CHANNEL;
    public static String BROADCAST_CHANNEL;
    public static String DEFAULT_RANK;
    public static Map<String, Object> DISCORD_ROLES;
    public static boolean BROADCAST_DISCORD_TO_ALL_SERVERS;
    public static long AMAP_INACTIVITY_BEFORE_DATA_UNLOAD;
    public static long AMAP_INACTIVITY_BEFORE_PLAYER_UNLOAD;

    public static void reload(MoonCore plugin) {
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();
        SERVER_NAME = config.getString("server-name");
        SERVER_DISCORD_PREFIX = config.getString("server-discord-prefix","");
        SERVER_MINECRAFT_PREFIX = ColorTranslator.translateAlternateColorCodes(config.getString("server-minecraft-prefix",""));
        BROADCAST_DISCORD_TO_ALL_SERVERS = config.getBoolean("discord.broadcast-to-all-servers",true);
        DEFAULT_RANK = config.getString("discord.global-rank");
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "en_US.yml");

        BOT_TOKEN = config.getString("bot-token", "");
        CHAT_CHANNEL = config.getString("channel.chat", "");
        CONSOLE_CHANNEL = config.getString("channel.console", "");
        DEBUG_CHANNEL = config.getString("channel.debug", "");
        BROADCAST_CHANNEL = config.getString("channel.broadcast","");
        COLOR_LOGS = config.getBoolean("color-logs", true);

        DISCORD_ROLES = config.getConfigurationSection("discord.roles").getValues(false);

        AMAP_INACTIVITY_BEFORE_DATA_UNLOAD = 1000*config.getLong("amap-inactivity-before-data-unload",1800);
        AMAP_INACTIVITY_BEFORE_PLAYER_UNLOAD = 1000*config.getLong("amap-inactivity-before-player-unload",8);
    }
}
