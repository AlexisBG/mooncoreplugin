package moonlit.alexis.mooncore.listener;

import com.vdurmont.emoji.EmojiParser;
import moonlit.alexis.mooncore.Logger;
import moonlit.alexis.mooncore.MoonCore;
import moonlit.alexis.mooncore.configuration.Config;
import moonlit.alexis.mooncore.configuration.Lang;
import moonlit.alexis.mooncore.util.ColorTranslator;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;

import net.dv8tion.jda.api.events.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import bsh.Interpreter;
import static org.bukkit.Bukkit.getLogger;





public class JDAListener extends ListenerAdapter {
    private final MoonCore plugin;
    private TextChannel debugChannel;
    Interpreter interpreter;

    public JDAListener(MoonCore plugin) {
        this.plugin = plugin;
        this.interpreter = new Interpreter();
        try{
            this.interpreter.eval("import *;");
        }
        catch(Exception e){
            getLogger().info("An unknown error occured building JDA..");
            e.printStackTrace();
        }

    }

    @Override
    public void onReady(ReadyEvent event) {
        Logger.debug("Discord: Connected");

        TextChannel channel;

        
        if (Config.CONSOLE_CHANNEL != null && !Config.CONSOLE_CHANNEL.isEmpty()) {
            channel = event.getJDA().getTextChannelById(Config.CONSOLE_CHANNEL);
            if (channel != null) {
                plugin.getBot().setConsoleChannel(channel);
            } else {
                Logger.error("Could not register console channel!");
            }
        }

        if (Config.CHAT_CHANNEL != null && !Config.CHAT_CHANNEL.isEmpty()) {
            channel = event.getJDA().getTextChannelById(Config.CHAT_CHANNEL);
            if (channel != null) {
                plugin.getBot().setChatChannel(channel);
                plugin.getBot().sendMessageToDiscord(Lang.SERVER_ONLINE);
            } else {
                Logger.error("Could not register chat channel!");
            }
        }

        if (Config.BROADCAST_CHANNEL != null && !Config.BROADCAST_CHANNEL.isEmpty()) {
            TextChannel broadcastChannel = event.getJDA().getTextChannelById(Config.BROADCAST_CHANNEL);
            if (broadcastChannel != null) {
                plugin.getBot().setBroadcastChannel(broadcastChannel);
            } else {
                Logger.error("Not using broadcast channel.");
            }
        }

        if (Config.DEBUG_CHANNEL != null && !Config.DEBUG_CHANNEL.isEmpty()) {
            debugChannel = event.getJDA().getTextChannelById(Config.DEBUG_CHANNEL);
            if (debugChannel != null) {
                plugin.getBot().setDebugChannel(debugChannel);
            } else {
                Logger.error("Could not register debug channel!");
            }
        }
    }

    @Override
    public void onResume(ResumedEvent event) {
        Logger.debug("Discord: Resumed connection");
        setChannel(event.getJDA());
    }

    @Override
    public void onReconnect(ReconnectedEvent event) {
        Logger.debug("Discord: Re-connected");
        setChannel(event.getJDA());
    }

    @Override
    public void onDisconnect(DisconnectEvent event) {
        Logger.debug("Discord: Disconnected");
        setChannel(null);
    }

    @Override
    public void onShutdown(ShutdownEvent event) {
        Logger.debug("Discord: Shutting down");
        setChannel(null);
    }

    private void setChannel(JDA jda) {
        TextChannel channel = jda != null ? jda.getTextChannelById(Config.CHAT_CHANNEL) : null;
        plugin.getBot().setChatChannel(channel);
    }

    class Console {
        public String consoleOutput = "";
    
        public void log(String arg){
            consoleOutput += "\n" + arg;
        }
    
        public String printAll(){
            return consoleOutput.replace("\n$", "");
        }
    
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String debugMessage = event.getMessage().getContentRaw();
        Member debugUser = event.getMember();

        if (event.getMessage().getChannel().getId().equals(Config.BROADCAST_CHANNEL)){
            String msgContent = event.getMessage().getContentRaw();
            if (!(msgContent.startsWith("§§") && (msgContent.split("§§")[1].split("§§")[0].equals(Config.SERVER_NAME)))){
                Lang.sendToAllPlayers(msgContent.split("§§",3)[2], false, false);
            }
        }
        if (event.getAuthor() == event.getJDA().getSelfUser()) {
            return; // dont echo
        }
        if (event.isWebhookMessage()) {
            return; // do not listen to webhooks
        }
        if (event.getMessage().getChannel().getId().equals(Config.CHAT_CHANNEL)) {
            String content = event.getMessage().getContentRaw();
            if (content.startsWith("!") && content.length() > 1) {
                String[] split = content.split(" ");
                String command = split[0].substring(1).toLowerCase();
                String[] args = Arrays.copyOfRange(split, 1, split.length);
                plugin.getBot().handleCommand(event.getAuthor().getName(), command, args);
            } else {
                StringBuilder message = new StringBuilder(EmojiParser.parseToAliases(event.getMessage().getContentDisplay()));
                List<Message.Attachment> att = event.getMessage().getAttachments();
                for (Message.Attachment attachment : att.subList(0, att.size() > 3 ? 3 : att.size())) {
                    if (message.length() > 0) {
                        message.append(" ");
                    }
                    message.append(attachment.getUrl());
                }
                Map<String, Object> roles = Config.DISCORD_ROLES;
                Member user = event.getMember();

                String roleColor = "";
                String roleName = "";
                int position = 0;
                for (Role urole : user.getRoles()){
                    int roleposition = urole.getPosition();
                    if (roleposition > position){
                        String ID = urole.getId();
                        if (roles.containsKey(ID)){
                            String val = roles.get(ID).toString();
                            roleColor = (val.isEmpty()) ? "<#"+Integer.toHexString(urole.getColorRaw())+">" : val;
                            position = roleposition;
                            roleName = urole.getName().replaceAll("[^\\w\\s\\+\\-*\\/\\^\\&\\:\\•\\.\\\\|\\(\\)\\>\\<\\[\\]\\{\\}]","").trim();
                        }

                    }
                }
                String toSend = Lang.DISCORD_CHAT_FORMAT
                    .replace("{userPrefix}",roleColor)
                    .replace("{userColor}",ColorTranslator.extractFormats(roleColor))
                    .replace("{userName}",event.getAuthor().getName())
                    .replace("{userDiscriminator}",event.getAuthor().getDiscriminator())
                    .replace("{roleName}",roleName)
                    .replace("{message}", message.toString());

                toSend = ColorTranslator.translateAlternateColorCodes(toSend);

                Lang.sendToAllPlayers(toSend,false,false);
                if (Config.BROADCAST_DISCORD_TO_ALL_SERVERS){
                    Lang.sendToAllProxyPlayers(toSend,null,false,false);
                    
                }
            }
        } else if (event.getMessage().getChannel().getId().equals(Config.CONSOLE_CHANNEL)) {
            if (event.getAuthor() == null || event.getAuthor().getId() == null || plugin.getBot().getClient().getSelfUser().getId() == null || event.getAuthor().getId().equals(plugin.getBot().getClient().getSelfUser().getId())) {
                return;
            }
            new BukkitRunnable() {
                public void run() {
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), event.getMessage().getContentRaw().replace("@here","@.here").replace("@everyone","@.everyone").replace("<@&","<@.&"));
                }
            }.runTask(plugin);
        } else if (event.getMessage().getChannel().getId().equals(debugChannel.getId())) { //will be replaced by a config option later on
                String debugresult = "```java\n" + debugMessage + "``````";
                int lines = ( debugMessage.split("\n", -1).length );
                Console console = new Console();
                try {
                    this.interpreter.set("message", event.getMessage());
                    this.interpreter.set("author", debugUser);
                    this.interpreter.set("console", console);
                    this.interpreter.set("Core", this.plugin);

                    debugresult += this.interpreter.eval(debugMessage).toString();
                }

                catch (NullPointerException e) {
                    if (lines > 1) {
                        debugresult += "Done";
                    }
                    else {
                        debugresult += "None";
                    }
                }
                catch (Exception e) {
                    debugresult += "[ERROR] " + e.toString();
                }
                debugresult += console.printAll() + "```";
                debugChannel.sendMessage(debugresult).queue();
        }
    }


    // public void registerEvents(Bot bot) {
    //     Events.get().register(new Events.Listener() {
    //         @Override
    //         public void entryAdded(Entry entry) {
    //             TextChannel minecraftLogsChannel = bot.getClient().getTextChannelById(Config.MODLOGS_CHANNEL);
    //             if (minecraftLogsChannel != null) {
    //                 String reason = entry.getReason();
    //                 String moderatorName = entry.getExecutorName();
    //                 Date time = new Date();
    //                 String formattedTime = (new SimpleDateFormat("HH:mm:ss")).format(time);
    //                 long untilvalue = entry.getDateEnd();
    //                 String formattedUntil;


    //                 if (entry.isPermanent()){
    //                     formattedUntil = "for **permanently**";
    //                 } else {
    //                     formattedUntil = "for **" + TimeUtil.formatDateDiff(untilvalue, entry.getDateStart()) + "**";
    //                 }
                    
    //                 String verb; String color; String username;

    //                 try{
    //                     ConfigurationSection punishmentConfig = Config.MODLOGS.getConfigurationSection(entry.getType());
    //                     verb = punishmentConfig.getString("verb");
    //                     color = punishmentConfig.getString("color").toUpperCase().replace("#","");
    //                 } catch (Exception e){
    //                     verb = entry.getType();
    //                     color = "000000";
    //                 }

    //                 try{
    //                     username = Bukkit.getOfflinePlayer(UUID.fromString(entry.getUuid())).getName();
    //                 } catch (Exception e){
    //                     username = entry.getUuid();
    //                 }

    //                 EmbedBuilder embed = new EmbedBuilder();
    //                 embed.setColor(Integer.parseInt(color, 16));
    //                 embed.setAuthor(username, null, "https://www.mc-heads.net/avatar/" + username + ".png");
    //                 embed.setTitle("`" + username + "` has been __" + (entry.isIpban() ? "ip" : "") + verb + "__ by `" + moderatorName + "`:");
    //                 String description = "";
    //                 description += "**·** " + formattedUntil;
    //                 description += "\n>>> " + reason;
    //                 embed.setDescription(description);
    //                 embed.setFooter(formattedTime + " | " + entry.getUuid());
    //                 minecraftLogsChannel.sendMessage(embed.build()).queue();
    //             }
    //         }
    //     });
    // }

}
