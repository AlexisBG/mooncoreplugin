package moonlit.alexis.mooncore.listener;

// import moonlit.alexis.mooncore.Logger;
import moonlit.alexis.mooncore.MoonCore;
import moonlit.alexis.mooncore.configuration.Config;
import moonlit.alexis.mooncore.configuration.Lang;
import moonlit.alexis.mooncore.util.ExpUtil;
import moonlit.alexis.mooncore.util.ColorTranslator;
import org.bukkit.*;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_19_R1.inventory.CraftItemStack;
import org.bukkit.craftbukkit.v1_19_R1.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;

import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.event.world.ChunkLoadEvent;

import java.util.HashSet;
import java.util.HashMap;
import java.util.List;
import java.util.Collection;
import java.util.Set;
import java.util.Map;
import java.util.ArrayList;
import java.util.Iterator;


import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerJoinEvent;


import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;

import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerItemMendEvent;

// import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.game.ClientboundMapItemDataPacket;
// import net.minecraft.network.protocol.game.PacketPlayOutSetSlot;
// import net.minecraft.network.protocol.game.PacketPlayOutEntityEquipment;
// import net.minecraft.network.protocol.game.PacketPlayOutWindowItems;
// import net.minecraft.world.level.saveddata.maps.WorldMap.b;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;

// import net.minecraft.world.entity.ai.attributes.
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.MapMeta;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.entity.Item;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.GlowItemFrame;
import org.bukkit.entity.ArmorStand;
import java.awt.Image;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.net.URLConnection;
import org.bukkit.map.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.metadata.IIOMetadataNode;
import org.w3c.dom.NodeList;


import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import  javax.imageio.metadata.IIOMetadata;

import javax.imageio.ImageReader;

import java.io.InputStream;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.bukkit.boss.BossBar;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BarFlag;
import org.bukkit.Particle;

class Dithering {
    private Dithering() {
    }

    public static void applyFloydSteinbergDithering(BufferedImage image){
        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                final int argb = image.getRGB(x, y);
                final int a = (argb >> 24) & 0xff;
                final int r = (argb >> 16) & 0xff;
                final int g = (argb >> 8) & 0xff;
                final int b = argb & 0xff;
                final byte index = MapPalette.matchColor(r, g, b);
                // final int index = palette.getPaletteIndex(argb);
                final int nextArgb = MapPalette.getColor(index).getRGB();
                // final int nextArgb = palette.getEntry(index);
                image.setRGB(x, y, nextArgb);

                final int na = (nextArgb >> 24) & 0xff;
                final int nr = (nextArgb >> 16) & 0xff;
                final int ng = (nextArgb >> 8) & 0xff;
                final int nb = nextArgb & 0xff;

                final int errA = a - na;
                final int errR = r - nr;
                final int errG = g - ng;
                final int errB = b - nb;

                if (x + 1 < image.getWidth()) {
                    int update = adjustPixel(image.getRGB(x + 1, y), errA, errR, errG, errB, 7);
                    image.setRGB(x + 1, y, update);
                    if (y + 1 < image.getHeight()) {
                        update = adjustPixel(image.getRGB(x + 1, y + 1), errA, errR, errG, errB, 1);
                        image.setRGB(x + 1, y + 1, update);
                    }
                }
                if (y + 1 < image.getHeight()) {
                    int update = adjustPixel(image.getRGB(x, y + 1), errA, errR, errG, errB, 5);
                    image.setRGB(x, y + 1, update);
                    if (x - 1 >= 0) {
                        update = adjustPixel(image.getRGB(x - 1, y + 1), errA, errR, errG, errB, 3);
                        image.setRGB(x - 1, y + 1, update);
                    }

                }
            }
            try{Thread.sleep(1);} catch (Exception e){}
        }
    }

    private static int adjustPixel(final int argb, final int errA, final int errR, final int errG, final int errB, final int mul) {
        int a = (argb >> 24) & 0xff;
        int r = (argb >> 16) & 0xff;
        int g = (argb >> 8) & 0xff;
        int b = argb & 0xff;

        a += errA * mul / 16;
        r += errR * mul / 16;
        g += errG * mul / 16;
        b += errB * mul / 16;

        if (a < 0) {
            a = 0;
        } else if (a > 0xff) {
            a = 0xff;
        }
        if (r < 0) {
            r = 0;
        } else if (r > 0xff) {
            r = 0xff;
        }
        if (g < 0) {
            g = 0;
        } else if (g > 0xff) {
            g = 0xff;
        }
        if (b < 0) {
            b = 0;
        } else if (b > 0xff) {
            b = 0xff;
        }

        return (a << 24) | (r << 16) | (g << 8) | b;
    }
}

class PictureCreatingQueue{
    public static ArrayList<PictureCreatingQueue> all = new ArrayList<PictureCreatingQueue>();
    public Player player;
    public volatile PlayerDropItemEvent lastRequest;
    public static String barTitle = ColorTranslator.translateAlternateColorCodes("<#ff6114>&lGenerating Maps: &r<#4842f9>&o");
    public BossBar progressBar = Bukkit.createBossBar("", BarColor.BLUE, BarStyle.SOLID, new BarFlag[0]);

    public PictureCreatingQueue(Player player){
        this.player = player;
        all.add(this);
    }

    public static PictureCreatingQueue fromPlayer(Player player){
        for (PictureCreatingQueue queue:all){
            if (queue.player.equals(player)){
                return queue;
            }
        }
        return new PictureCreatingQueue(player);
    }

    public void initializeProgressBar(Player dropper){
        progressBar.setTitle(barTitle+"Adapting the image...");
        progressBar.setProgress(0);
        progressBar.addPlayer(dropper);
    }

    public void updateProgressBar(int currentFrame,int frameAmount,int currentMap, int mapNeededAmount){
        progressBar.setTitle(barTitle+"Frame "+Integer.toString(currentFrame+1)+"/"+Integer.toString(frameAmount)+", Map "+Integer.toString(currentMap+1)+"/"+Integer.toString(mapNeededAmount)+"...");
        progressBar.setProgress(((double)(currentFrame*mapNeededAmount+currentMap))/((double)(mapNeededAmount*frameAmount)));
    }

    public void setFinalStatus(){
        progressBar.setProgress(1);
        progressBar.setTitle(barTitle+"Saving maps & adding them to inv...");
    }

    public void finish(){
        progressBar.removeAll();
    }
}


class AnimatedPicGroupHolders{
    public volatile List<AnimatedPicGroup> groups = new ArrayList<AnimatedPicGroup>();
    public String holder;
    public long initialized = System.currentTimeMillis();
    public long period=0;
    public static List<AnimatedPicGroupHolders> holders = new ArrayList<AnimatedPicGroupHolders>();
    public ExecutorService thread = Executors.newSingleThreadExecutor();

    public AnimatedPicGroupHolders(String holderName){
        this.holder = holderName;
        this.thread.execute(()->{
            Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
            while (true){
                try{
                    this.render();
                    Thread.sleep(120);} catch (Exception e){}
            }

        });

    }

    public void render(){
        // if (AnimatedPicGroupHolders.debuglevel2()) System.out.println("Rendering holder "+this.holder+" "+this.toString());
        Iterator<AnimatedPicGroup> groupsIterator = this.groups.iterator();
        
        while (groupsIterator.hasNext()){
            AnimatedPicGroup group = groupsIterator.next();
            long t = System.currentTimeMillis();
            this.period = t-this.initialized;

            if (group.inactiveSince==0){
                if (group.players.isEmpty()) group.inactiveSince = this.period;
                else{
                    group.render();
                    try{Thread.sleep(System.currentTimeMillis()-t);} catch (Exception e){}
                }
            } else {
                if (group.players.isEmpty()){
                    if (this.period-group.inactiveSince>Config.AMAP_INACTIVITY_BEFORE_DATA_UNLOAD){
                        groupsIterator.remove();
                        group.holder=null;
                        group.inactiveSince=0;
                        AnimatedPicGroup.all.remove(group);
                        for (AnimatedPic pic:group.pictures){
                            for (MapRenderer renderer:pic.map.getRenderers()){
                                pic.map.removeRenderer(renderer);
                                pic.loader = Integer.toString(pic.map.getId());
                                pic.group = AnimatedPicGroup.getOrCreateGroup(group.name);
                            }
                            pic.initData();
                            pic.map.addRenderer(new Renderer(pic));
                        }

                    }
                } else{
                    group.inactiveSince=0;
                }
            }
            // if (AnimatedPicGroupHolders.debuglevel2()) System.out.println("Group "+group.name+" "+group.toString()+" Period: "+Long.toString(this.period));
            

        }
    }

    // public static void startLoop(){
    //     if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("starting loop");
    //     Thread loop = new Thread(()->{
    //         Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
    //         if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("in thread");
    //         while (true){
    //             //to remove later, debug
    //             File f = new File(MoonCore.getInstance().getDataFolder(), "custom_maps/temp/activated");
    //             if (!f.exists()) continue;

    //             //end of debug
    //             try{if (AnimatedPicGroupHolders.debuglevel2()) System.out.println("SLEEEEEEEEEEEEEPING NOW");Thread.sleep(100);} catch (Exception e){if (AnimatedPicGroupHolders.debuglevel2()) e.printStackTrace();}
    //             // System.out.println("looping again");
    //             // System.out.println(all.size());
    //             if (AnimatedPicGroupHolders.debuglevel2()) System.out.println("===================");
    //             try{
    //                 Iterator<AnimatedPicGroupHolders> holdersIterator = holders.iterator();
    //                 while (holdersIterator.hasNext()){
    //                     AnimatedPicGroupHolders holder = holdersIterator.next();
    //                     if (AnimatedPicGroupHolders.debuglevel2()) System.out.println("holder: "+holder.holder+" "+holder.toString());
    //                     holder.render();
    //                     // try{Thread.sleep(2000);} catch (Exception e){}
    //                 }
    //             } catch (Exception e){
    //                 if (AnimatedPicGroupHolders.debuglevel3()) e.printStackTrace();
    //             }

    //         }
    //     });
    //     loop.start();
    // }

    // public static boolean debuglevel3(){
    //     File f = new File(MoonCore.getInstance().getDataFolder(), "custom_maps/temp/debug3");
    //     return f.exists();
    // }

    // public static boolean debuglevel2(){
    //     File f = new File(MoonCore.getInstance().getDataFolder(), "custom_maps/temp/debug2");
    //     return (debuglevel3() || f.exists());
    // }

    // public static boolean debuglevel1(){
    //     File f = new File(MoonCore.getInstance().getDataFolder(), "custom_maps/temp/debug1");
    //     return (debuglevel3() || debuglevel2() || f.exists());
    // }
    
}

class AnimatedPicGroup{
    public List<AnimatedPic> pictures = new ArrayList<AnimatedPic>();
    public AnimatedPic main;
    public AnimatedPicGroupHolders holder;
    public int totalLength = 0;
    public List<Integer> delays = new ArrayList<Integer>();
    public List<Integer> cSumDelays = new ArrayList<Integer>();
    public String name;
    public int currentIndex = 0;
    public short answersNeeded = 0;
    HashMap<CraftPlayer, Long> lastViewed = new HashMap<CraftPlayer, Long>();
    public long inactiveSince = 0;
    public List<CraftPlayer> players = new ArrayList<CraftPlayer>();
    public static List<AnimatedPicGroup> all = new ArrayList<AnimatedPicGroup>();
    // public static ExecutorService pool = Executors.newCachedThreadPool();
    

    public AnimatedPicGroup(String name){
        // if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("being createdddd");
        // if (AnimatedPicGroupHolders.debuglevel1()) System.out.println(all.toString());
        this.name = name;
        all.add(this);
    }

    public boolean catchupDelays(){
        // String debug = "delays catchup:";
        
        long period = this.holder.period%this.totalLength;
        // debug += "\n"+"Period: "+Long.toString(period)+" | Total Length: "+Integer.toString(this.totalLength);
        int i = currentIndex;
        // debug += "\n"+Arrays.toString(this.cSumDelays.toArray());
        // debug += "\n"+"Current Index: "+Integer.toString(i) + " | Total Frames: "+Integer.toString(this.main.frameAmount);
        int nextI;
        // if (i==this.main.frameAmount-1){
        //     debug+= " | last index: ";
        //     if (period<this.cSumDelays.get(i)){debug+="New Index: 0";i=0; }
        //     else{debug += "staying"+"\n"+"Updated: false";return false;} 
        // }
        // while (period>=this.cSumDelays.get(nextI=(i<this.main.frameAmount-1 ? i+1 : 0)) ){//(nextI=i+1)<this.main.frameAmount
        //     i = nextI;
        // }
        if (period<this.cSumDelays.get(i)) i=0;
        while ((nextI=i+1)<this.main.frameAmount && period>=this.cSumDelays.get(nextI)) i=nextI;



        // debug += "New Index: "+Integer.toString(i);

        boolean updated = currentIndex!=i;
        // debug += "\nUpdated: "+Boolean.toString(updated);
        this.currentIndex=i;
        // if (AnimatedPicGroupHolders.debuglevel3()) System.out.println(debug);
        return updated;

    }

    public synchronized void addImage(AnimatedPic pic){
        // if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("adding image");
        // if (AnimatedPicGroupHolders.debuglevel1()) System.out.println(pic);
        if (this.main==null){
            this.main = pic;
            this.main.frameAmount = this.main.delays.size();
            for (int delay:this.main.delays) {
                int newDelay = delay*10;
                this.delays.add(newDelay);
                this.cSumDelays.add(this.totalLength);
                this.totalLength += newDelay;
                
            }
        }
        pic.group = this;
        this.pictures.add(pic);

    }

    public void setHolder(String holderName){
        AnimatedPicGroupHolders newHolder=null;
        for (AnimatedPicGroupHolders groupHolder:AnimatedPicGroupHolders.holders){
            if (groupHolder.holder.equals(holderName)){
                newHolder = groupHolder;
                break;
            }
        }
        if (newHolder==null) newHolder = new AnimatedPicGroupHolders(holderName);
        this.holder = newHolder;
        newHolder.groups.add(this);
        AnimatedPicGroupHolders.holders.add(newHolder);
        
    }

    public synchronized void forceRender(AnimatedPic img){
        Iterator<CraftPlayer> pIterator = this.players.iterator();
        ClientboundMapItemDataPacket packet = img.packets.get(this.currentIndex);
        while (pIterator.hasNext()){
            pIterator.next().getHandle().connection.send(packet);
        }

    }

    public synchronized void render(){
        // if (AnimatedPicGroupHolders.debuglevel2()) System.out.println("Rendering group "+this.name+" "+this.toString()+" from holder "+this.holder.holder+" "+this.holder.toString());
        // System.out.println("rendering");
        // long period = (System.currentTimeMillis()-this.initTime)%this.totalLength;
        // int currentIndex=0;
        // for (int i=this.img.frameAmount-1;(i>=0 && period<this.cSumDelays.get(i));i--){
        //     currentIndex = i;
        // }
        // System.out.println(currentIndex);
        // System.out.println(this.images.size());
        if (this.catchupDelays()){
            Iterator<AnimatedPic> imageIterator = this.pictures.iterator();
            while (imageIterator.hasNext()){ //thread safe FOREACH loop
                AnimatedPic pic = imageIterator.next();
                ClientboundMapItemDataPacket packet = pic.packets.get(this.currentIndex);
                Iterator<CraftPlayer> pIterator = this.players.iterator();
                while (pIterator.hasNext()){
                    CraftPlayer player = pIterator.next();
                    if (this.holder.period-this.lastViewed.get(player) > Config.AMAP_INACTIVITY_BEFORE_PLAYER_UNLOAD){
                        pIterator.remove();
                        this.lastViewed.remove(player);
                        continue;
                    }
                    player.getHandle().connection.send(packet);
                }

                
                
                // System.out.println(oimg.canvas);
                
                // if (oimg.canvas!= null){
                //     this.answersNeeded += 1;
                //     pool.execute(()->{
                //         //((CraftPlayer) player).getHandle().playerConnection.networkManager.sendPacket(new ClientboundMapItemDataPacket(yourMapId, 4, false, true, new ArrayList<>(), yourByteArray,0,0,128,128));
                //         oimg.canvas.drawImage(0, 0, oimg.frames.get(currentIndex));
                //         this.answersNeeded -= 1;
                //     });
                // } 
                
            }

        }


    }

    public static AnimatedPicGroup getOrCreateGroup(String name){
        for (AnimatedPicGroup group:all){
            if (group.name.equals(name)) return group;
        }
        return new AnimatedPicGroup(name);
    }




}

// class Renderer extends MapRenderer {
//     public BufferedImage image;
//     public Picture pic;
//     public boolean rendered = false;
//     public AnimatedPicGroup group;
//     public String holderName;
    
//     public Renderer(BufferedImage image){
//         this.image = image;
//     }

//     public Renderer(ImageLoader oimg,String groupName,String holderName){
//         //ImageLoader oimg,String groupName,String holderName
//         if (!(oimg.isAnimated)){
//             this.image = oimg.frames.get(0);
//         } else{
//             this.img = oimg;
//             this.group = AnimatedPicGroup.getOrCreateGroup(groupName);
//             this.holderName = holderName;
            

//         }
//     }

//     public void render(MapView map,MapCanvas canvas,Player player){
//         if (this.image!=null){
//             if (this.rendered){
//                 return;
//             }
//             threadPool.execute(() -> canvas.drawImage(0, 0, (Image)image));
//             this.rendered = true;
                    
//         } else {
//             CraftPlayer craftplayer = (CraftPlayer)player;
//             if (!(this.group.players.contains(craftplayer))) this.group.players.add(craftplayer);

//             if (!this.rendered){
//                 this.rendered = true;
//                 threadPool.execute(()->{
//                     try{
//                         this.img.preparePackets(map.getId());
//                         group.addImage(this.img);
//                         if (this.img.group.holder==null) this.img.group.setHolder(this.holderName);
//                         this.group.forceRender(this.img);
//                     } catch (IOException e){
//                         e.printStackTrace();
//                     } 
                    
//                 });
//             }

//             // System.out.println(this.img.toString()+" "+before+" "+this.img.canvas.toString());


//         }

//     }

// }

class Renderer extends MapRenderer {
    public Picture pic;
    boolean rendered_once = false;
    public static ExecutorService threadPool = Executors.newFixedThreadPool(3);

    public Renderer(Picture pic){
        this.pic = pic;
    }

    public void render(MapView map,MapCanvas canvas,Player player){
        if (!(this.rendered_once)){
            this.rendered_once = true;
            threadPool.execute(()->{
                Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
                if (this.pic.loader!=null){
                    this.pic.load();
                }
                this.pic.firstRender(map, canvas, player);
            });

        }
        this.pic.render(map,canvas,player);
    }

}

abstract class Picture{
    // public int mapID;
    public MapView map;
    public String loader;


    public Picture(Player player,String mapId){
        this.loader = mapId;
        if (mapId==null){
            this.map = Bukkit.createMap(player.getWorld());
        } else {
            this.map = Bukkit.getMap(Integer.parseInt(mapId));
        }
        this.map.setLocked(true);
        this.map.getRenderers().clear();

    }

    public void startRendering(){
        this.map.addRenderer(new Renderer(this));
    }

    public abstract void addFrame(BufferedImage frame,int delay);//Declare how to add a frame to the picture

    public abstract void write();//Declare how to save the picture in a file

    public abstract void firstRender(MapView map,MapCanvas canvas,Player player); //Actions to do when a map is rendered for the very first time.

    public void render(MapView map,MapCanvas canvas,Player player){};//Optional, this is what is executed every time minecraft tries to render the map again. (runs again every couple of ticks while map is viewable by a player)

    public abstract void load(); //define what to do in case the picture constructs with an ImageLoader; this function would trigger just before firstRender

    

}

class AnimatedPic extends Picture{

    public List<byte[]> packetsData = new ArrayList<byte[]>();
    public List<ClientboundMapItemDataPacket> packets = new ArrayList<ClientboundMapItemDataPacket>();
    public AnimatedPicGroup group;
    public String holderName;
    public int frameAmount;
    public List<Integer> delays = new ArrayList<Integer>();
    public String storageId;

    public void initData(){
        this.packetsData = new ArrayList<byte[]>();
        this.packets = new ArrayList<ClientboundMapItemDataPacket>();
        this.delays = new ArrayList<Integer>();
    }

    public AnimatedPic(Player player, int frameAmount,String groupId, String holderName,String mapId){
        super(player,mapId);
        this.frameAmount = frameAmount;
        mapId = (mapId==null) ? Integer.toString(this.map.getId()) : mapId;
        if (groupId==null) groupId = mapId;
        this.holderName = (holderName==null) ? player.getName() : holderName;
        this.group = AnimatedPicGroup.getOrCreateGroup(groupId);
        this.storageId = mapId+"-"+groupId+"-"+this.holderName;
        // File savePath = new File(MoonCore.getInstance().getDataFolder(), "custom_maps/"+ fileName +".gif");
        // try{
        //     writer.setOutput(new FileImageOutputStream(savePath));
        //     writer.prepareWriteSequence(null);
        // } catch (IOException e){
        //     if (AnimatedPicGroupHolders.debuglevel1()) e.printStackTrace();
        // }
    }

    public void firstRender(MapView map,MapCanvas canvas, Player player){
        this.group.addImage(this);
        if (this.group.holder==null) this.group.setHolder(this.holderName);
        // if (AnimatedPicGroupHolders.debuglevel3()) System.out.println(AnimatedPicGroupHolders.holders);
        this.group.forceRender(this);
    
    }

    public void render(MapView map,MapCanvas canvas,Player player){
        CraftPlayer craftplayer = (CraftPlayer)player;
        if (this.group!=null && this.group.main==this){
            this.group.lastViewed.put(craftplayer, this.group.holder.period);
            if (!(this.group.players.contains(craftplayer))) this.group.players.add(craftplayer);
        }
    }


    public void addFrame(BufferedImage frame,int delay){
        byte[] packetData = MapPalette.imageToBytes(frame);
        this.packetsData.add(packetData);
        this.packets.add(
            new ClientboundMapItemDataPacket(this.map.getId(), (byte)4, false, new ArrayList<>(),new net.minecraft.world.level.saveddata.maps.MapItemSavedData.MapPatch(0,0,128,128,packetData))
        );
        
        this.delays.add(delay);
        // try{
        //     IIOMetadata metadata = this.gifReader.getImageMetadata(currentIndex);

        //     writer.writeToSequence(new IIOImage(frame,null,metadata), null);
        // } catch (IOException e){
        //     if (AnimatedPicGroupHolders.debuglevel1()) e.printStackTrace();
        // }

    }

    public void write(){
        try{
            FileOutputStream f = new FileOutputStream(new File(MoonCore.getInstance().getDataFolder(), "custom_maps/"+ this.storageId));
            ObjectOutputStream o = new ObjectOutputStream(f);
            o.writeObject(this.packetsData);
            o.writeObject(this.delays);
            o.close();
            f.close();
        } catch (Exception e){e.printStackTrace();}
        
        // try{
        //     writer.endWriteSequence();
        // } catch (IOException e){
        //     if (AnimatedPicGroupHolders.debuglevel1()) e.printStackTrace();
        // }
        
    }

    public void load(){
        try{
            FileInputStream fi = new FileInputStream(new File(MoonCore.getInstance().getDataFolder(), "custom_maps/"+ this.storageId));
            ObjectInputStream oi = new ObjectInputStream(fi);
            List<byte[]> data = (List<byte[]>)oi.readObject();
            Thread.sleep(50);
            this.delays = (List<Integer>)oi.readObject();
            oi.close();
            fi.close();
            this.frameAmount = data.size();
            for (int i=0;i<this.frameAmount;i++){
                Thread.sleep(5);
                this.packets.add(
                    new ClientboundMapItemDataPacket(this.map.getId(), (byte)4, false, new ArrayList<>(),new net.minecraft.world.level.saveddata.maps.MapItemSavedData.MapPatch(0,0,128,128,data.get(i)))
                );
            }

            
            
        } catch (Exception e){
        }
        

    }

}
class StaticPic extends Picture{
    BufferedImage image;
    public StaticPic(Player player,String mapId){
        super(player,mapId);
    }

    public void addFrame(BufferedImage image,int null1){
        this.image = image;
    }

    public void write(){
        try{
            File savePath = new File(MoonCore.getInstance().getDataFolder(), "custom_maps/"+ this.map.getId() +".png");
            ImageIO.write(image,"png",savePath);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public void firstRender(MapView map,MapCanvas canvas, Player player){
        canvas.drawImage(0, 0, (Image)this.image);
        this.image = null;//deleting image from memory after render
        try{Thread.sleep(50);} catch (Exception e){}
    }

    public void load(){
        try{
            InputStream inputStream = new FileInputStream(new File(MoonCore.getInstance().getDataFolder(), "custom_maps/"+ this.map.getId() +".png"));
            this.image = ImageIO.read(inputStream);
            Thread.sleep(50);
        } catch (Exception e){
            
        }
        
    }

}


class ImageLoader{
    public int frameAmount;
    public int currentIndex = 0;
    public int width = -1;
    public int height = -1;
    public BufferedImage main;
    public int minIndex;
    public int lastDelay;
    public boolean isAnimated = false;
    public ImageReader gifReader;
    public MapView mapView;
    public boolean ready = false;
    public AnimatedPicGroup group;
    public IIOMetadata metadata;
    public BufferedImage master = null;
    public Graphics2D masterGraphics = null;
    public Picture picture;

    public ImageLoader(String imgURL,boolean animated) throws IOException {
        //don't forget to prepareanimatedpic befr usage
        // if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("using the new system"); 
        URLConnection connection = new URL(imgURL).openConnection();
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        connection.connect();
        InputStream inputStream = connection.getInputStream();

        if (animated){
            ImageReader reader = ImageIO.getImageReadersBySuffix("gif").next();
            reader.setInput(ImageIO.createImageInputStream(inputStream));
            this.gifReader = reader;
            this.metadata = this.gifReader.getStreamMetadata();
            this.minIndex = reader.getMinIndex();
            this.frameAmount = reader.getNumImages(true);
            this.main = reader.read(minIndex);
            if (this.frameAmount>=2) this.isAnimated = true;
            

        } else {
            this.frameAmount = 1;
            this.main = ImageIO.read(inputStream);

        }
    }

    public void prepareAnimatedPic() throws IOException{
        if (this.isAnimated){

            if (metadata != null) {
                IIOMetadataNode globalRoot = (IIOMetadataNode) metadata.getAsTree(metadata.getNativeMetadataFormatName());
        
                NodeList globalScreenDescriptor = globalRoot.getElementsByTagName("LogicalScreenDescriptor");
        
                if (globalScreenDescriptor != null && globalScreenDescriptor.getLength() > 0) {
                    IIOMetadataNode screenDescriptor = (IIOMetadataNode) globalScreenDescriptor.item(0);
        
                    if (screenDescriptor != null) {
                        width = Integer.parseInt(screenDescriptor.getAttribute("logicalScreenWidth"));
                        height = Integer.parseInt(screenDescriptor.getAttribute("logicalScreenHeight"));
                    }
                }
            }
            this.currentIndex = 0;
        }
    }

    public boolean hasNextFrame(){
        return (this.isAnimated ? (this.currentIndex<this.frameAmount) : (this.currentIndex==0));
    }
    

    public BufferedImage nextFrame() throws IOException{
        if (this.isAnimated){
            int actualIndex = (this.currentIndex+this.minIndex)%this.frameAmount;
            BufferedImage image;
            image = this.gifReader.read(actualIndex);


            if (width == -1 || height == -1) {
                this.width = image.getWidth();
                this.height = image.getHeight();
            }

            IIOMetadataNode root = (IIOMetadataNode) this.gifReader.getImageMetadata(actualIndex).getAsTree("javax_imageio_gif_image_1.0");
            IIOMetadataNode gce = (IIOMetadataNode) root.getElementsByTagName("GraphicControlExtension").item(0);
            int delay = Integer.valueOf(gce.getAttribute("delayTime"));
            String disposal = gce.getAttribute("disposalMethod");

            int x = 0;
            int y = 0;

            if (master == null) {
                master = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
                masterGraphics = master.createGraphics();
                masterGraphics.setBackground(new java.awt.Color(0, 0, 0, 0));
            } else {
                NodeList children = root.getChildNodes();
                for (int nodeIndex = 0; nodeIndex < children.getLength(); nodeIndex++) {
                    Node nodeItem = children.item(nodeIndex);
                    if (nodeItem.getNodeName().equals("ImageDescriptor")) {
                        NamedNodeMap map = nodeItem.getAttributes();
                        x = Integer.valueOf(map.getNamedItem("imageLeftPosition").getNodeValue());
                        y = Integer.valueOf(map.getNamedItem("imageTopPosition").getNodeValue());
                    }
                }
            }
            masterGraphics.drawImage(image, x, y, null);

            BufferedImage copy = new BufferedImage(master.getColorModel(), master.copyData(null), master.isAlphaPremultiplied(), null);
            // this.frames.add(copy);
            this.lastDelay = delay;

            if (disposal.equals("restoreToPrevious")) {
                //if (!this.disposals.get(i).equals("restoreToPrevious")
                BufferedImage from = null;
                from = this.main;
                // for (int i = frameIndex - 1; i >= 0; i--) {
                //     if (!this.disposals.get(i).equals("restoreToPrevious") || frameIndex == 0) {
                //         from = this.frames.get(i);
                //         break;
                //     }
                // }

                master = new BufferedImage(from.getColorModel(), from.copyData(null), from.isAlphaPremultiplied(), null);
                masterGraphics = master.createGraphics();
                masterGraphics.setBackground(new java.awt.Color(0, 0, 0, 0));
            } else{
                if (disposal.equals("restoreToBackgroundColor")) {
                    masterGraphics.clearRect(x, y, image.getWidth(), image.getHeight());
                }
                this.main = copy;
            }
            this.currentIndex += 1;
            return copy; 
        } else {
            this.currentIndex = 1;
            return this.main;
        }

    }

    public List<Picture> createSubPictureTemplates(Player player,int amount){
        amount = (amount>0 ? amount : 1);
        List<Picture> pictures = new ArrayList<Picture>();
        String playerName = player.getName();
        if (this.isAnimated){
            AnimatedPic firstAnimatedPicture = new AnimatedPic(player, this.frameAmount,null,playerName,null);//Player player,ImageReader gifReader, int frameAmount,String groupId, String holderName,ImageLoader loader
            pictures.add(firstAnimatedPicture);
            String groupName = Integer.toString(firstAnimatedPicture.map.getId());
            for (int i=0;i<amount-1;i++){
                pictures.add(new AnimatedPic(player, this.frameAmount,groupName,playerName,null));
            }

        } else {
            for (int i=0;i<amount;i++){
                pictures.add(new StaticPic(player,null));
            }
        }
        return pictures;

  }

}

class MulticolorArmorStep{
    public float duration;
    public Color color;
    public String mode;

    public MulticolorArmorStep(Color color, String mode,float duration){

        this.color = color;
        this.mode = mode;
        this.duration = duration;
    }

}

class MulticolorArmorGroup{
    public String id;
    public List<MulticolorArmor> members = new ArrayList<MulticolorArmor>();
    public static List<MulticolorArmorGroup> queue = new ArrayList<MulticolorArmorGroup>();
    public static boolean queueStarted = false;
    public long lastAnimated = -1;
    public long firstAnimated = -1;


    public MulticolorArmorGroup(MulticolorArmor firstMember){
        this.id = firstMember.playerID;
        this.members.add(firstMember);
        queue.add(this);
        this.start();
    }

    public void add(MulticolorArmor member){
        if (this.lastAnimated!=-1){
            member.updateTimerAndStep(((float)(System.currentTimeMillis()-this.firstAnimated)/1000)%member.totalDuration);
        }
        this.members.add(member); //awo uwu
    }

    public void remove(MulticolorArmor member){
        this.members.remove(member);
        if (this.members.isEmpty()){
            this.lastAnimated = -1;
            this.firstAnimated = -1;
        }
    }

    public void start(){        
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(MoonCore.getInstance(), ()->{
            try{
                long now = System.currentTimeMillis();
                float ellapsed;
                if (this.lastAnimated==-1){
                    ellapsed = 0;
                    this.firstAnimated = now;
                } else {
                    ellapsed = (float)(now-this.lastAnimated)/1000;
                }
                try{

                    for (MulticolorArmor armor:this.members){
                        armor.updateToNextColor(ellapsed);
                    }
                    this.lastAnimated = now;
                } catch (Exception uwu){System.out.println("xd4");uwu.printStackTrace();}
            } catch (Exception i){i.printStackTrace();}
        },3,3);


    }

}

class AnvilArmorCreatingProcess{
    public boolean runningThread = false;
    public MulticolorArmor armor;

    public AnvilArmorCreatingProcess(MulticolorArmor armor){
        this.armor = armor;
    }
}

class MulticolorArmor{
    public ItemStack armor;
    public List<MulticolorArmorStep> steps = new ArrayList<MulticolorArmorStep>();
    public MulticolorArmorStep currentStep;
    public MulticolorArmorStep nextStep;
    public int nextStepIndex;
    public int maxStepIndex;
    public float totalDuration = 0;
    public float counter = 0;
    public LeatherArmorMeta meta;
    public String playerID;
    private static Pattern p = Pattern.compile("#([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})\\s+([fF][aA][dD][eE]|[sS][tT][aA][yY])\\s+(\\d+(?:\\.\\d+)?)");
    public static HashMap<String,AnvilArmorCreatingProcess> creationQueue = new HashMap<String,AnvilArmorCreatingProcess>();
    public long uniqueID;
    public boolean active = false;
    public Runnable extraChangingColorActions;
    public boolean inventoryLock = false;
    public boolean stopped = false;
    public boolean metaWritingLock = false;

    public MulticolorArmor(ItemStack item, String playerID,String settings) throws Exception{
        // Pattern p = Pattern.compile("#([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})\\s+([fF][aA][dD][eE]|[sS][tT][aA][yY])\\s+(\\d+(?:\\.\\d+)?)");
        String[] lines = settings.split("\n");
        if (lines.length < 2){
            throw new Exception("§cError: the multicolor armor must include at least 2 steps (corresponding each to a different color), otherwise it is not multicolor but staying on one same color (that you can archieve already by simply dyeing a leather armor in a crafting table.\n§cMake sure that your configuration contains at least 2 lines, one step per line. The following example will make a piece of armor gradiate from red to blue in a cycle:\n§2#ff0000 fade 2\n§2#0000ff fade 2");
        }
        boolean allColorsIdentical = true;
        Color lastColor = null;
        for (int i=0;i<lines.length;i++){
            Matcher m = p.matcher(lines[i]);
            if (m.find()){
                float duration = Float.parseFloat(m.group(5));
                if (duration < 0.3 || duration >600){
                    throw new Exception("§cValue error at line "+Integer.toString(i+1)+": the duration must be between 0.3 and 600 seconds. PS: never add any time unit next to the duration, the value is always treated as seconds.");
                }
                Color color = Color.fromRGB(Integer.parseInt(m.group(1),16), Integer.parseInt(m.group(2),16), Integer.parseInt(m.group(3),16));
                if (allColorsIdentical && lastColor!=null && lastColor != color) allColorsIdentical=false;
                this.steps.add(
                    new MulticolorArmorStep(
                        color,
                        m.group(4).toLowerCase(),
                        duration
                    )
                );
                this.totalDuration += duration;
                lastColor = color;
            } else {
                throw new Exception("§cSyntax error at line "+Integer.toString(i+1)+": couldn't match the multicolor armor step pattern #HEXCODE fade/stay duration. Example: §2#00bdff fade 3.5\n§cPS: You are not obliged to sign the book, this way you can fix your errors without rewriting a whole book.");
            }
        }
        if (allColorsIdentical) throw new Exception("Error: every multicolor armor step have the same color, this corresponds to a single-color piece of armor which you can simply craft in the crafting table. Please make sure to include at least 2 different colors in your multicolor armor configuration.");
        net.minecraft.world.item.ItemStack nms = CraftItemStack.asNMSCopy(item);
        CompoundTag nbt = nms.hasTag() ? nms.getTag() : new CompoundTag();
        this.uniqueID = System.currentTimeMillis();
        nbt.putString("multicolor",settings);
        nbt.putString("multicolor-playerid",playerID);
        nbt.putLong("multicolor-uniqueID",this.uniqueID);
        nms.setTag(nbt);
        item.setItemMeta(CraftItemStack.asBukkitCopy(nms).getItemMeta());
        this.armor = item;
        this.playerID = playerID;
        this.initialize();
    }

    private MulticolorArmor(){
        //Create an empty object from MulticolorArmor.fromMulticolorArmorItem constructor, which initializes the attributes itself.
    }

    public static boolean isMulticolorArmorItem(ItemStack item){
        if (item==null) return false;
        net.minecraft.world.item.ItemStack nms = CraftItemStack.asNMSCopy(item);
        if (nms.hasTag()){
            if (nms.getTag().contains("multicolor")){
                return true;
            }
        }
        return false;
    }

    public static void runWithMetaLock(ItemStack item,Runnable runnable){
        if (isMulticolorArmorItem(item)){
            MulticolorArmor multicolorArmor = MulticolorArmor.fromMulticolorArmorItem(item);
            //while (multicolorArmor.metaWritingLock); // blocks the main thread until the lock is off: ensures thread safety and synchronous actions to an item's meta
            try{
                multicolorArmor.metaWritingLock = true;
                runnable.run();
                multicolorArmor.armor = item;
            } catch (Exception exception){
            } finally {
                multicolorArmor.metaWritingLock = false;
            }
        } else {
            runnable.run();
        }
    }

    public void setItem(ItemStack item){
        //while (this.metaWritingLock);
        try{
            this.metaWritingLock = true;
            this.armor = item;
        } catch (Exception e){   
        } finally{
            this.metaWritingLock = false;
        }
    }

    public static void updateInventoryWithMulticolorArmor(Inventory inv){
        ItemStack newItem;
        MulticolorArmor piece;
        for (int i=0;i<inv.getSize();i++){
            newItem = inv.getItem(i);
            if (MulticolorArmor.isMulticolorArmorItem(newItem)){
                piece = MulticolorArmor.fromMulticolorArmorItem(newItem);
                piece.start();
            }
        }
    }

    public static void updateInventoryWithMulticolorArmor(InventoryView inv){
        ItemStack newItem;
        MulticolorArmor piece;
        for (int i=0;i<inv.countSlots();i++){
            newItem = inv.getItem(i);
            if (MulticolorArmor.isMulticolorArmorItem(newItem)){
                piece = MulticolorArmor.fromMulticolorArmorItem(newItem);
                piece.start();
            }
        }
    }


    public static MulticolorArmor fromMulticolorArmorItem(ItemStack item){
        return fromMulticolorArmorItem(item, true);
    }

    public static MulticolorArmor fromMulticolorArmorItem(ItemStack item,boolean updateItem){
        net.minecraft.world.item.ItemStack nms = CraftItemStack.asNMSCopy(item);
        CompoundTag nbt = nms.getTag();
        Long uniqueID = nbt.getLong("multicolor-uniqueID");
        for (MulticolorArmorGroup group:MulticolorArmorGroup.queue){
            for (MulticolorArmor member:group.members){
                if (member.uniqueID == uniqueID){
                    if (updateItem){
                        member.extraChangingColorActions = null;
                        member.setItem(item);
                    }
                    return member;
                }
            }
        }

        MulticolorArmor  obj = new MulticolorArmor();
        String settings = nbt.getString("multicolor");
        obj.playerID = nbt.getString("multicolor-playerid");
        obj.uniqueID = uniqueID;
        Matcher m = p.matcher(settings);
        while (m.find()){
            float duration = Float.parseFloat(m.group(5));
            obj.steps.add(
                new MulticolorArmorStep(
                    Color.fromRGB(Integer.parseInt(m.group(1),16), Integer.parseInt(m.group(2),16), Integer.parseInt(m.group(3),16)),
                    m.group(4).toLowerCase(),
                    duration
                )
            );
            obj.totalDuration += duration;
        }
        obj.armor = item;
        obj.initialize();
        return obj;
    }

    public void initialize(){
        this.meta = (LeatherArmorMeta) this.armor.getItemMeta();
        this.nextStepIndex = 1;
        this.currentStep = this.steps.get(0);
        this.maxStepIndex = this.steps.size()-1;
        this.nextStep = this.steps.get(1);

    }

    public void addToInventory(Inventory inv){
        ItemStack newItem;
        for (int i=0;i<inv.getSize();i++){
            newItem = inv.getItem(i);
            if (newItem==null){
                inv.setItem(i,this.armor);
                this.armor = inv.getItem(i);
                this.start();
                return;
            }

        }
    }

    public void next(){
        this.nextStepIndex = this.nextStepIndex+1<=this.maxStepIndex ? this.nextStepIndex+1 : 0;
        this.currentStep = this.nextStep;
        this.nextStep = this.steps.get(this.nextStepIndex);
    }


    public void pause(){
        this.active = false;
        for (MulticolorArmorGroup group:MulticolorArmorGroup.queue){
            if (group.id.equals(this.playerID)){
                group.remove(this);
                return;
            }
        }
    }

    public void start(){
        if (!(this.active)){
            this.active = true;
            for (MulticolorArmorGroup group:MulticolorArmorGroup.queue){
                if (group.id.equals(this.playerID)){
                    group.add(this);
                    return;
                }
            }
            new MulticolorArmorGroup(this);
        }
    }

    public void start(Runnable extraActions){
        this.start();
        this.extraChangingColorActions = extraActions;
    }

    public void changeColor(Color newcolor){
        //if (this.metaWritingLock) return; //doesn't wait for the lock to be off: ensures that the multicolorarmor queue isn't blocked; every piece are checked in a cycle so we can hope that the lock will be off at a later check
        try{
            this.metaWritingLock = true;
            this.meta = (LeatherArmorMeta)this.armor.getItemMeta();
            if (this.meta.getColor().equals(newcolor)) return;
            this.meta.setColor(newcolor);
            this.armor.setItemMeta(this.meta);
        } catch (Exception e){
        } finally {
            this.metaWritingLock=false;
        }


    }


    public void updateTimerAndStep(float ellapsed){
        this.counter += ellapsed;
        while (this.counter>=this.currentStep.duration){
            this.counter = this.counter-this.currentStep.duration;
            this.next();
        }

    }

    public void updateToNextColor(float ellapsed){
        // System.out.println("sdsds"+Float.toString(ellapsed)+this.currentStep.mode+" "+this.nextStepIndex+" "+this.counter);
        try{
            this.updateTimerAndStep(ellapsed);
            // System.out.println("sdsds2"+this.currentStep.mode+" "+this.nextStepIndex+" "+this.counter);
            if (this.currentStep.mode.equals("stay")){
                // if (false){//this.nextStepIndex==previousNextStepIndex){
                    // System.out.println("awa");
                    //The color is already up to date, because the color is supposed to stay as long as the duration of the step, which is still the same since last check.
                    // return;
                // } else{
                    // System.out.println("Changing color");
                    // System.out.println(this.currentStep.color);
                    this.changeColor(this.currentStep.color);
                // }
            } else {
                float factor = (this.counter/this.currentStep.duration);
                this.changeColor(
                    Color.fromRGB(
                        (int)(this.currentStep.color.getRed()*(1-factor)+this.nextStep.color.getRed()*factor),
                        (int)(this.currentStep.color.getGreen()*(1-factor)+this.nextStep.color.getGreen()*factor),
                        (int)(this.currentStep.color.getBlue()*(1-factor)+this.nextStep.color.getBlue()*factor)
                    )
    
                );
            }
    
            if (this.extraChangingColorActions != null) this.extraChangingColorActions.run();

        } catch (Exception e){e.printStackTrace();}

    }

}



public class PlayerListener implements Listener {
    private final Set<PlayerTeleportEvent.TeleportCause> backAllowCauses = new HashSet<>();
    private String[] tools = new String[] {"_SWORD","_HELMET","_CHESTPLATE","_BOOTS","_LEGGINGS","_HORSE_ARMOR"};
    public static ExecutorService threadPool = Executors.newFixedThreadPool(3);
    //private HashMap<String,ArrayList<ImageLoader>> imageGroups = new HashMap<String,ArrayList<ImageLoader>>();



    public PlayerListener() {
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        backAllowCauses.add(PlayerTeleportEvent.TeleportCause.PLUGIN);
        backAllowCauses.add(PlayerTeleportEvent.TeleportCause.COMMAND);
        backAllowCauses.add(PlayerTeleportEvent.TeleportCause.UNKNOWN);
        try{
        File folder = new File(MoonCore.getInstance().getDataFolder(), "custom_maps");
        for (File file:folder.listFiles()){
            String fullFileName = file.getName();
            String fileName = fullFileName.replaceFirst("[.][^.]+$", "");
            if(!(fileName.matches("\\d+") || fileName.matches("\\d+-\\d+-\\w+"))){
                System.out.println(fileName+" doesn't match requirements...");
                continue;
            }

            // String[] parts = fileName.split("-");
            // backupMapId = Integer.parseInt(parts[0]);
            // backupMapView = Bukkit.getMap(backupMapId);
            
            

            try{
                String[] parts = fileName.split("-");
                Picture picture = fullFileName.contains("-") ? (new AnimatedPic(null,0,parts[1],parts[2],parts[0])) : new StaticPic(null,parts[0]);
                picture.startRendering();

            } catch (Exception e){
                
            }


        }
    } catch (Exception e){
        e.printStackTrace();
    }

    // AnimatedPicGroupHolders.startLoop();

    }


    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        player.setFallDistance(-1024F); // do not fall to death on login


        // event.setJoinMessage(null);
        // Bukkit.broadcastMessage(ChatColor.GOLD + player.getName() + ChatColor.DARK_GRAY + " has joined the server!");


        event.getPlayer().setCustomName(ColorTranslator.translateAlternateColorCodes(MoonCore.getInstance().getPlayerPrefix(player) + player.getName()));



    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        // Possible fix for enderchests merging with viewer
        player.getEnderChest().getViewers().forEach(viewer -> {
            viewer.closeInventory();
        });
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onChangeWorld(PlayerChangedWorldEvent event) {
        // Possible fix for enderchests merging with viewer
        event.getPlayer().getEnderChest().getViewers().forEach(viewer -> {
            viewer.closeInventory();
        });
    }





    @EventHandler
    public void onRightClickWithMending(PlayerInteractEvent event) {
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK && event.getAction() != Action.RIGHT_CLICK_AIR) {
            return; // not a right click
        }
        ItemStack item = event.getItem();
        Player player = event.getPlayer();
        if (MulticolorArmor.isMulticolorArmorItem(item)){
            event.setCancelled(true);
            if (event.getAction() == Action.RIGHT_CLICK_AIR){
                EntityEquipment equipment = player.getEquipment();
                EquipmentSlot pieceType=null;
                String name = item.getType().name();
                if (name.endsWith("HELMET") && equipment.getHelmet()==null){
                    pieceType = EquipmentSlot.HEAD;
                } else if (name.endsWith("BOOTS") && equipment.getBoots()==null){
                    pieceType = EquipmentSlot.FEET;
                } else if (name.endsWith("CHESTPLATE") && equipment.getChestplate()==null){
                    pieceType = EquipmentSlot.CHEST;
                } else if (name.endsWith("LEGGINGS") && equipment.getLeggings()==null){
                    pieceType = EquipmentSlot.LEGS;
                }

                if (pieceType!=null){
                    equipment.setItem(event.getHand(),null);
                    equipment.setItem(pieceType,item);
                    MulticolorArmor piece = MulticolorArmor.fromMulticolorArmorItem(equipment.getItem(pieceType));
                    piece.start();
                }
            }

        }
        
        if (!player.isSneaking()) {
            return; // not sneaking
        }
        
        if (item == null || item.getType() == org.bukkit.Material.AIR) {
            return; // not holding item
        }
        short damage = item.getDurability();
        if (damage <= 0) {
            return; // not damaged
        }
        if (item.getEnchantmentLevel(Enchantment.MENDING) <= 0) {
            return; // not enchanted with mending
        }
        if (player.getTotalExperience() < 2 && player.getLevel() <= 0) {
            return; // not enough experience
        }
        item.setDurability((short) (damage - 4));
        ExpUtil.addPlayerXP(player, -2);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void chat(final AsyncPlayerChatEvent event) {
        //"&7[&b{serverName}&7] {playerPrefix}{playerName}{playerSuffix}{message}"
        event.setCancelled(true);
        Player player = event.getPlayer();
        String playerMessage = event.getMessage();

        // if (event.getMessage().startsWith("!")) {
        //     if (player.hasPermission("channels.staff.moderator")) {
        //         for (ProxiedPlayer wp:MoonCore.getInstance().proxy.getPlayers()) {

        //             if (wp.hasPermission("channels.staff.moderator")) {
        //                 String channel_staff = "<#03fcc2>[StaffChat] ";
        //                 wp.sendMessage(ColorTranslator.translateAlternateColorCodes(channel_staff + player.getName() + ": " + "&r" + playerMessage.replaceAll("!", "")));
        //             }
        //         }
        //     } else {
        //         player.sendMessage(ChatColor.DARK_RED + "No permissions");
        //     }
        // } else 
        // if (playerMessage.contains(".nl") || playerMessage.contains(".com") || playerMessage.contains("play.") || (playerMessage.contains("server.") && !player.hasPermission("mooncore.bypass"))) {
        //     //Anti-ads
        //     player.sendMessage(ChatColor.RED + "Please do not advertise!");
        //     for (ProxiedPlayer wp:MoonCore.getInstance().proxy.getPlayers()) {
        //         if (wp.hasPermission("mooncore.notify"))
        //             wp.sendMessage(ChatColor.RED + player.getName() + ChatColor.GRAY + " tried to advertise (" + event.getMessage() + ")");
        //     } 
        // } else {

        String prefix = MoonCore.getInstance().getPlayerPrefix(player);
        String displayName = player.getDisplayName();
        String newMessage = Lang.MINECRAFT_CHAT_FORMAT
        .replace("{playerPrefix}",prefix)
        .replace("{playerName}",displayName)
        .replace("{playerSuffix}",MoonCore.getInstance().getPlayerSuffix(player))
        .replace("{message}",playerMessage)
        .replace("{playerColor}",ColorTranslator.extractFormats(prefix+displayName));
        newMessage = ColorTranslator.translateAlternateColorCodes(newMessage);
        Lang.sendToAllPlayers(newMessage,false,true);
        Lang.sendToAllProxyPlayers(newMessage,player,false,true);

        // }
    }

    @EventHandler
    public void onPlayerEditBook(PlayerEditBookEvent book){
        BookMeta meta = book.getNewBookMeta().clone();
        List<String> pages = new ArrayList<>();
        try {
            meta.setTitle(ColorTranslator.translateAlternateColorCodes(meta.getTitle()));
        } catch (Exception e) {}
        if (pages.size() > 0 && pages.get(0).toLowerCase().startsWith("http")) {
            return;
        }
        for (String page:meta.getPages()){
            pages.add(ColorTranslator.translateAlternateColorCodes(page));
        }
        meta.setPages(pages);       
        book.setNewBookMeta(meta);


    }

    @EventHandler
    public void onPlayerItemRename(PrepareAnvilEvent anvil){
        AnvilInventory inv = anvil.getInventory();
        ItemStack item = anvil.getResult();
        if (item==null) item = inv.getResult();
        String uuid = anvil.getView().getPlayer().getUniqueId().toString();
        if (MulticolorArmor.creationQueue.containsKey(uuid)){
            AnvilArmorCreatingProcess anvilProcess = MulticolorArmor.creationQueue.get(uuid);
            if (anvilProcess!=null && !(anvilProcess.runningThread)){
                    anvilProcess.runningThread = true;
                    Thread thread = new Thread(() ->{
                        Thread.currentThread().setPriority(Thread.MIN_PRIORITY);

                        // 
                            while (MulticolorArmor.creationQueue.containsKey(uuid)){
                                try{
                                    ItemStack first = inv.getFirstItem();
                                    ItemStack second = inv.getSecondItem();
                                    if (first == null || second == null) break;
                                    String firstType =first.getType().name();
                                    String secondType = second.getType().name();
                                    if (!((secondType == "WRITTEN_BOOK" || secondType == "WRITABLE_BOOK") && firstType.startsWith("LEATHER_"))) break;
                                } catch (Exception owo){}

                                try{
                                    inv.setResult(anvilProcess.armor.armor);
                                } catch (Exception owo){}
                            }
                            inv.setResult(null);
                            anvilProcess.armor.pause();
                            MulticolorArmor.creationQueue.remove(uuid);

                    });
                    thread.start();

                
                return;
            }

        }
        if (item == null){
            ItemStack initialArmor = inv.getFirstItem();
            String toolType = "";
            
            
            ItemStack second = inv.getSecondItem();
            if (initialArmor == null || second == null) return;
            String firstType =initialArmor.getType().name();
            String secondType = second.getType().name();
            if ((secondType == "WRITTEN_BOOK" || secondType == "WRITABLE_BOOK") && firstType.startsWith("LEATHER_") && !(MulticolorArmor.isMulticolorArmorItem(initialArmor))){
                
                if (MulticolorArmor.creationQueue.containsKey(uuid)){
                    return;
                }
                MulticolorArmor.creationQueue.put(uuid,null);
                BookMeta meta = (BookMeta) second.getItemMeta();
                if (meta.getPageCount() != 0){
                    try {
                        MulticolorArmor result = new MulticolorArmor(initialArmor.clone(),anvil.getView().getPlayer().getUniqueId().toString(),meta.getPage(1));
                        result.updateToNextColor(0);
                        result.start();
                        anvil.setResult(result.armor);
                        MulticolorArmor.creationQueue.put(uuid,new AnvilArmorCreatingProcess(result));
                        
                    } catch (Exception e){
                        anvil.getView().close();
                        anvil.getView().getPlayer().sendMessage(e.getMessage());
                    }
                    

                }
                return;

                
            }

            if (secondType.equals("OBSIDIAN") && (firstType.endsWith("CHESTPLATE") || firstType.endsWith("LEGGINGS") || firstType.endsWith("BOOTS") || firstType.endsWith("HELMET"))){

                ItemStack newItem = initialArmor.clone();

                String nbtEnchants = "";
                
                List<String> newLore = newItem.getLore();
                if (newLore==null) newLore = new ArrayList<String>();
                Map<Enchantment,Integer> enchantments = newItem.getEnchantments();
                int[] nbtEnchantLevels = new int[enchantments.size()];
                int i = 0;
                for (Enchantment ench:enchantments.keySet()){
                    String enchName = ench.getKey().getKey();
                    int level = enchantments.get(ench);
                    nbtEnchants += enchName + "\n";
                    nbtEnchantLevels[i] = level;
                    newLore.add(enchName + " "+Integer.toString(level));
                    newItem.removeEnchantment(ench);
                    i++;
                }
                net.minecraft.world.item.ItemStack nms = CraftItemStack.asNMSCopy(newItem);
                CompoundTag tag = nms.hasTag() ? nms.getTag() : new CompoundTag();
                if (nbtEnchants.endsWith("\n")) nbtEnchants = nbtEnchants.substring(0,nbtEnchants.length()-1);
                tag.putString("fake-enchant-names",nbtEnchants);
                tag.putIntArray("fake-enchant-levels", nbtEnchantLevels);
                tag.putLong("fake-enchant-last-triggered",System.currentTimeMillis()-3000);
                nms.setTag(tag);
                newItem = CraftItemStack.asBukkitCopy(nms);
                newItem.setLore(newLore);
                anvil.setResult(newItem);
                return;

            }



            for (String type:tools){
                if (firstType.endsWith(type)) {
                    toolType = type;break;
                }
            }
            if (toolType!="" && secondType.endsWith(toolType) && secondType!=firstType){
                Material newArmorType = second.getType();
                ItemStack newArmor = initialArmor.clone();
                List<String> lore = newArmor.hasLore() ? newArmor.getLore() : new ArrayList<String>();
                short maxDurability = initialArmor.getType().getMaxDurability();
                int actualDamage = initialArmor.getDamage();
                newArmor.setType(newArmorType);
                net.minecraft.world.item.ItemStack nmsInitial = CraftItemStack.asNMSCopy(initialArmor);
                net.minecraft.world.item.ItemStack nmsNew = CraftItemStack.asNMSCopy(newArmor);
                CompoundTag tag = nmsNew.hasTag() ? nmsNew.getTag() : new CompoundTag();
                tag.putShort("actual-maxdurability",maxDurability);
                tag.putInt("actual-damage", actualDamage);
                tag.putInt("lore-durability-line", lore.size());
                lore.add("Durability: "+Integer.toString(maxDurability-actualDamage)+" / "+Integer.toString(maxDurability));
                
                nmsNew.setTag(tag);
                nmsNew.setDamageValue(actualDamage*newArmor.getType().getMaxDurability()/maxDurability);
                
                for (net.minecraft.world.entity.EquipmentSlot slot:net.minecraft.world.entity.EquipmentSlot.values()){
                    Map<net.minecraft.world.entity.ai.attributes.Attribute,Collection<AttributeModifier>> all = nmsInitial.getAttributeModifiers(slot).asMap();
                    for (net.minecraft.world.entity.ai.attributes.Attribute attr:nmsInitial.getAttributeModifiers(slot).keySet()){
                        for (AttributeModifier mod:all.get(attr)){
                            nmsNew.addAttributeModifier(attr, mod, slot);
                        }
                    }
                }
                newArmor = CraftItemStack.asBukkitCopy(nmsNew);
                for (ItemFlag flag:initialArmor.getItemFlags()){
                    newArmor.addItemFlags(flag);
    
                }
                newArmor.setLore(lore);
                anvil.setResult(newArmor);

            }
            return;

        }

        int initialDamage = inv.getFirstItem().getDamage();
        int newDamage = item.getDamage();
        if (initialDamage != newDamage){
            ItemStack item2 = item;
            MulticolorArmor.runWithMetaLock(item, () -> {
                net.minecraft.world.item.ItemStack nmsFinal= CraftItemStack.asNMSCopy(item2);
                if (nmsFinal.hasTag()){
                    CompoundTag nbt = nmsFinal.getTag();
                    if (nbt.contains("actual-maxdurability")){
                        short maxDurability = nbt.getShort("actual-maxdurability");
                        int actualDamage = nbt.getInt("actual-damage");
                        int loreLineIndex = nbt.getInt("lore-durability-line");
                        actualDamage = maxDurability*newDamage/item2.getType().getMaxDurability();
                        nbt.putInt("actual-damage", actualDamage);
                        nmsFinal.setTag(nbt);
                        nmsFinal.setDamageValue(actualDamage*item2.getType().getMaxDurability()/maxDurability);
                        List<String> lore = item2.getLore();
                        List<String> newLore = new ArrayList<String>();
                        for (int i=0;i<lore.size();i++){
                            newLore.add(i!=loreLineIndex ? lore.get(i) : "Durability: "+Integer.toString(maxDurability-actualDamage)+" / "+Integer.toString(maxDurability));
                        }
                        ItemStack newItem = CraftItemStack.asBukkitCopy(nmsFinal);
                        item2.setItemMeta(newItem.getItemMeta());
                        item2.setLore(newLore);
                        anvil.setResult(item2);return;
                }
            }

            });

    }
        
        ItemMeta meta = item.getItemMeta();
        if (inv.contains(Material.ZOMBIE_HEAD) || inv.contains(Material.SKELETON_SKULL) || inv.contains(Material.CREEPER_HEAD) || inv.contains(Material.WITHER_SKELETON_SKULL)){
            ItemStack skull = new ItemStack(Material.PLAYER_HEAD);
            SkullMeta skullmeta = (SkullMeta) skull.getItemMeta();

            skullmeta.setOwningPlayer(Bukkit.getOfflinePlayer(meta.getDisplayName()));
            skull.setItemMeta(skullmeta);
            anvil.setResult(skull);
        } else {
            meta.setDisplayName(ColorTranslator.translateAlternateColorCodes(meta.getDisplayName()));
            item.setItemMeta(meta);

        }
        String displayName = meta.getDisplayName();
        displayName = ColorTranslator.translateAlternateColorCodes(displayName);
        meta.setDisplayName(displayName);
        item.setItemMeta(meta);
    }

    @EventHandler
    public void invInteractMove(InventoryMoveItemEvent inv){
        ItemStack itemMove = inv.getItem();
        if (MulticolorArmor.isMulticolorArmorItem(itemMove)){
            MulticolorArmor piece = MulticolorArmor.fromMulticolorArmorItem(itemMove);
            piece.start();
        }
        // MulticolorArmor.updateInventoryWithMulticolorArmor(inv.getDestination());
    }


    

    @EventHandler
    public void anvilClick(InventoryClickEvent anvilEvent) {
        Inventory inv = anvilEvent.getInventory();
        InventoryView view = anvilEvent.getView();
        HumanEntity player = anvilEvent.getWhoClicked();
        // int slot = anvilEvent.getRawSlot();
        // String action = anvilEvent.getAction().name();

        if (inv.getType().equals(InventoryType.ANVIL) && anvilEvent.getSlot() == 2 && inv.getItem(2)!=null){
            ItemStack item = anvilEvent.getCurrentItem();
            net.minecraft.world.item.ItemStack nms = CraftItemStack.asNMSCopy(item);
            if (nms.hasTag()){
                CompoundTag nbt = nms.getTag();
                if (nbt.contains("actual-maxdurability") || nbt.contains("multicolor")){
                    
                    if (hasAvailableSlot(player.getInventory())){
                        inv.clear();
                        if (nbt.contains("multicolor")){
                            String uuid = player.getUniqueId().toString();
                            if (MulticolorArmor.creationQueue.containsKey(uuid)){
                                MulticolorArmor.creationQueue.remove(uuid);
                            }

                            view.setCursor(item);
                            MulticolorArmor piece = MulticolorArmor.fromMulticolorArmorItem(item);
                            piece.start(()->{
                                if (view.getCursor() != null) view.setCursor(item);
                            });
                            // piece.addToInventory(player.getInventory());

                            // player.getInventory().addItem(piece.armor);
                        } else{
                            view.setCursor(item);
                        }
                    }
                }

            }
    
            


        } else {


                ItemStack cacheCursor = view.getCursor();
                if (MulticolorArmor.isMulticolorArmorItem(cacheCursor)) MulticolorArmor.fromMulticolorArmorItem(cacheCursor);
        
                Bukkit.getScheduler().runTaskLater(MoonCore.getInstance(),()->{
                    InventoryView view2 = anvilEvent.getView().getPlayer().getOpenInventory();
                    ItemStack newItem;
                    MulticolorArmor invPiece;
                    net.minecraft.world.item.ItemStack  invNms;
                    if (view2==null) return;
                    for (int i=0;i<view2.countSlots();i++){
                        try{
                            newItem = view2.getItem(i);
                            if (MulticolorArmor.isMulticolorArmorItem(newItem)){
                                invNms = CraftItemStack.asNMSCopy(newItem);
                                if (newItem.hasEnchants() && invNms.hasTag() && invNms.getTag().contains("fake-enchant-names")){
                                    for (Enchantment ench:newItem.getEnchantments().keySet()){
                                        newItem.removeEnchant(ench);
                                    }
                                }
                                invPiece = MulticolorArmor.fromMulticolorArmorItem(newItem);
                                invPiece.start();
                            }
                        } catch (Exception ee){}

                    }


                    ItemStack cursor = view2.getCursor();
                    if (cursor==null || cursor.getType().isAir()) view2.setCursor(null);
                    else if (MulticolorArmor.isMulticolorArmorItem(cursor)){
                        MulticolorArmor piece = MulticolorArmor.fromMulticolorArmorItem(cursor);
                        view2.setCursor(piece.armor);
                        piece.start(() ->{
                            view2.setCursor(piece.armor);
                        });
                        // } else {
                            
                        // }

                    }
                }, 1);
            
            // if (anvilEvent.getClick().equals(ClickType.CREATIVE)) return;
            // System.out.println("item clicked!!!!!!!!!!!");
            // System.out.println(anvilEvent.getAction());
            // System.out.println(anvilEvent.getClick());

            // ItemStack cursor = view.getCursor();
            // ItemStack currentItem = anvilEvent.getCurrentItem();
            // MulticolorArmor multicolorCursor = null;
            // MulticolorArmor multicolorSlot = null;
            // if (MulticolorArmor.isMulticolorArmorItem(cursor)){
            //     multicolorCursor = MulticolorArmor.fromMulticolorArmorItem(cursor);
            //     // anvilEvent.setCancelled(true);
            //     if (multicolorCursor.inventoryLock) return;
            //     multicolorCursor.inventoryLock = true;
            // }
            // if (MulticolorArmor.isMulticolorArmorItem(currentItem)){
            //     multicolorSlot = MulticolorArmor.fromMulticolorArmorItem(currentItem);
            //     // anvilEvent.setCancelled(true);
            //     if (multicolorSlot.inventoryLock){
            //         if (multicolorCursor != null) multicolorCursor.inventoryLock = false;
            //         return;
            //     }
            //     multicolorSlot.inventoryLock = true;
            // }

            // if (anvilEvent.getAction().name().startsWith("PLACE")){
            //     if (multicolorCursor!=null){
            //         anvilEvent.setCancelled(true);
            //         view.setCursor(null);
            //         view.setItem(slot,cursor);
            //         ItemStack newItem = view.getItem(slot);
            //         multicolorCursor.armor = newItem;
            //         multicolorCursor.start();
                    
            //     }
            // } else if (anvilEvent.getAction().name().startsWith("PICKUP")){
            //     net.minecraft.world.item.ItemStack nms = CraftItemStack.asNMSCopy(currentItem);
            //     if (currentItem.hasEnchants() && nms.hasTag() && nms.getTag().contains("fake-enchant-names")){
            //         for (Enchantment ench:currentItem.getEnchantments().keySet()){
            //             currentItem.removeEnchant(ench);
            //         }
            //     }
            //     if (multicolorSlot!=null){
            //         multicolorSlot.armor = currentItem;
            //         anvilEvent.setCancelled(true);
            //         anvilEvent.setCurrentItem(null);
            //         MulticolorArmor piece = multicolorSlot;
            //         view.setCursor(piece.armor);
            //         multicolorSlot.start(() ->{
            //             ItemStack currentSlot = view.getItem(slot);
            //             if (currentSlot!=null && MulticolorArmor.isMulticolorArmorItem(currentSlot) && MulticolorArmor.fromMulticolorArmorItem(currentSlot).uniqueID==piece.uniqueID) view.setItem(slot,null);
            //             else view.setCursor(piece.armor);
                        
            //         });

            //     //     });
                    
            //     }
            // } else {
            //     if (multicolorCursor!=null || multicolorSlot!=null){
            //         Player owo = ((Player)anvilEvent.getView().getPlayer());
            //         owo.sendActionBar("§4This operation is not supported with multicolor armors, inventory shortcuts are disabled.");
            //         anvilEvent.setCancelled(true);


            //     }

            // }

            // if (multicolorSlot!=null) multicolorSlot.inventoryLock=false;
            // if (multicolorCursor!=null) multicolorCursor.inventoryLock=false;

        }



    }

    @EventHandler
    public void onArmorStandManipulate(PlayerArmorStandManipulateEvent event){
        ItemStack armorStandItem = event.getArmorStandItem();
        ItemStack playerItem = event.getPlayerItem();
        MulticolorArmor armorStandPiece=null;
        MulticolorArmor playerPiece=null;
        if (MulticolorArmor.isMulticolorArmorItem(armorStandItem)) armorStandPiece = MulticolorArmor.fromMulticolorArmorItem(armorStandItem);
        if (MulticolorArmor.isMulticolorArmorItem(playerItem)) playerPiece = MulticolorArmor.fromMulticolorArmorItem(playerItem);
        if (armorStandPiece==null && playerPiece==null) return;
        event.setCancelled(true);
        Player player = event.getPlayer();
        org.bukkit.inventory.PlayerInventory inv = player.getInventory();
        int playerSlot = inv.getHeldItemSlot();
        EquipmentSlot armorStandSlot = event.getSlot();
        ArmorStand armorStand = event.getRightClicked();
        EntityEquipment equipment = armorStand.getEquipment();

        if (armorStandItem!=null){ //picking
            armorStand.setItem(armorStandSlot, null);
            inv.setItem(playerSlot, armorStandItem);
            if (armorStandPiece != null){
                armorStandPiece.armor = inv.getItem(playerSlot);
                armorStandPiece.start();
            }

        } if (playerItem!=null){ //placing
            if (armorStandItem==null) inv.setItem(playerSlot,null);
            equipment.setItem(armorStandSlot, playerItem,true);
            if (playerPiece != null){
                MulticolorArmor playerPieceVar = playerPiece;
                playerPieceVar.start( ()->{
                    equipment.setItem(armorStandSlot, playerPieceVar.armor,true);
                });
            }

        }
    }

    @EventHandler
    public void chunkLoad(ChunkLoadEvent event){
        Entity[] entities = event.getChunk().getEntities();
        for (Entity entity:entities){
            if (entity instanceof HumanEntity){
                HumanEntity human = (HumanEntity)entity;
                MulticolorArmor.updateInventoryWithMulticolorArmor(human.getInventory());
            } else if (entity instanceof LivingEntity){
                LivingEntity livingEntity = (LivingEntity)entity;
                EntityEquipment equipment = livingEntity.getEquipment();
                for (EquipmentSlot pieceType:EquipmentSlot.values()){
                    ItemStack item = equipment.getItem(pieceType);
                    if (MulticolorArmor.isMulticolorArmorItem(item)){
                        equipment.setItem(pieceType,item);
                        MulticolorArmor piece = MulticolorArmor.fromMulticolorArmorItem(equipment.getItem(pieceType));
                        piece.start(()->{
                            equipment.setItem(pieceType,piece.armor,true);
                        });
                    }
                }
            }
        }
    }

    public boolean enchantFakeEnchantedPlayer(HumanEntity human){
        EntityEquipment equipment = human.getEquipment();
        boolean runExtraActions = false;
        long now = System.currentTimeMillis();
        for (EquipmentSlot pieceType:EquipmentSlot.values()){
            ItemStack item = equipment.getItem(pieceType);
            if (item==null) continue;
            item = item.clone();
            net.minecraft.world.item.ItemStack nms = CraftItemStack.asNMSCopy(item);
            if (nms.hasTag()){
                CompoundTag nbt = nms.getTag();
                if (nbt.contains("fake-enchant-names")){
                        String[] enchantNames = nbt.getString("fake-enchant-names").split("\\n");
                        int[] enchantLevels = nbt.getIntArray("fake-enchant-levels");
                        if ((!runExtraActions) && nbt.getLong("fake-enchant-last-triggered")+3000 <= now) runExtraActions = true;
                        nbt.putLong("fake-enchant-last-triggered",now);
                        nms.setTag(nbt);
                        ItemStack item2 = CraftItemStack.asBukkitCopy(nms);

                        //adding enchantements for a few seconds
                        for (int i=0;i<enchantNames.length;i++){
                            Enchantment ench = Enchantment.getByKey(NamespacedKey.minecraft(enchantNames[i]));
                            item2.addUnsafeEnchantment(ench, enchantLevels[i]);
                            
                        }
                        // System.out.println("dbg3");
                        equipment.setItem(pieceType,item2);
                        if (MulticolorArmor.isMulticolorArmorItem(item2)){
                            MulticolorArmor piece = MulticolorArmor.fromMulticolorArmorItem(equipment.getItem(pieceType));
                            // System.out.println("dbg3.5");
                            piece.start();
                        }


                        //scheduling auto removing enchantment in a few seconds
                        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(MoonCore.getInstance(),()->{
                            // System.out.println("dbg4");
                            EntityEquipment newEquipment = human.getEquipment();
                            ItemStack newItem = newEquipment.getItem(pieceType);
                            if (newItem==null) return;
                            newItem = newItem.clone();
                            
                            net.minecraft.world.item.ItemStack newNms = CraftItemStack.asNMSCopy(newItem);
                            if (!(newNms.hasTag())) return;
                            CompoundTag newNbt = newNms.getTag();
                            if (!(newNbt.contains("fake-enchant-last-triggered"))) return;
                            if ((newNbt.getLong("fake-enchant-last-triggered")+3000) <= System.currentTimeMillis()){
                                Set<Enchantment> enchantments = newItem.getEnchantments().keySet();
                                for (Enchantment newEnch:enchantments){
                                    newItem.removeEnchant(newEnch);
                                }
                                newEquipment.setItem(pieceType,newItem);
                                if (MulticolorArmor.isMulticolorArmorItem(newItem)){
                                    MulticolorArmor newPiece = MulticolorArmor.fromMulticolorArmorItem(newEquipment.getItem(pieceType));
                                    newPiece.start();
                                }
                                // System.out.println("dbg11");
                            }                             
                        },(long)300);
                }
            }

        }
        return runExtraActions;
    }

    @EventHandler
    public void onXpGain(PlayerExpChangeEvent event){
        HumanEntity human = (HumanEntity)event.getPlayer();
        if (enchantFakeEnchantedPlayer(human)){
            int currentAmount = event.getAmount();
            event.setAmount(0);
            event.getPlayer().giveExp(currentAmount,true);
        };
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent event){
        // tag.setString("fake-enchant-names",nbtEnchants);
        // tag.setIntArray("fake-enchant-levels", nbtEnchantLevels);
        // tag.putLong("fake-enchant-last-triggered",System.currentTimeMillis()-3000);
        Entity entity = event.getEntity();
        double currentAmount = event.getFinalDamage();

        if (entity instanceof HumanEntity){
            HumanEntity human = (HumanEntity)entity;
            if (enchantFakeEnchantedPlayer(human)){
                event.setCancelled(true);
                human.damage(currentAmount);
            };
        }
    }

    @EventHandler
    public void onConnect(PlayerJoinEvent event){
        MulticolorArmor.updateInventoryWithMulticolorArmor(event.getPlayer().getInventory());
    }

    @EventHandler
    public void onDrag(InventoryDragEvent event){
        if (MulticolorArmor.isMulticolorArmorItem(event.getOldCursor())){
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInvOpen(InventoryOpenEvent anvil){
        MulticolorArmor.updateInventoryWithMulticolorArmor(anvil.getInventory());
    }

    @EventHandler
    public void onInvClose(InventoryCloseEvent anvil){

        Inventory inv = anvil.getInventory();

        ItemStack first = inv.getItem(0);
        ItemStack second = inv.getItem(1);
        if (first == null || second == null) return;
        String secondType = second.getType().name();

        if (inv.getType().equals(InventoryType.ANVIL) && (secondType == "WRITTEN_BOOK" || secondType == "WRITABLE_BOOK") && first.getType().name().startsWith("LEATHER_")){
            String uuid = anvil.getPlayer().getUniqueId().toString();
            if (MulticolorArmor.creationQueue.containsKey(uuid)){
                MulticolorArmor.creationQueue.remove(uuid);
            }
        }

    }

    @EventHandler
    public void onItemDamage(PlayerItemDamageEvent damageEvent){
        ItemStack item = damageEvent.getItem();
        net.minecraft.world.item.ItemStack nms = CraftItemStack.asNMSCopy(item);
        if (nms.hasTag()){
            CompoundTag nbt = nms.getTag();
            if (nbt.contains("actual-maxdurability")){
                damageEvent.setCancelled(true);
                if (nbt.contains("multicolor")){
                    MulticolorArmor.fromMulticolorArmorItem(item);
                }
                // if (nbt.contains("fake-enchant-last-triggered") && enchantFakeEnchantedPlayer(damageEvent.getPlayer())){
                //     item.damage(damageEvent.getDamage());
                //     return;
                // }
                short actualMaxDurability = nbt.getShort("actual-maxdurability");
                int actualDamage = nbt.getInt("actual-damage");
                int loreLineIndex = nbt.getInt("lore-durability-line");
                actualDamage += damageEvent.getDamage();
                nbt.putInt("actual-damage", actualDamage);
                nms.setTag(nbt);
                int maxDurability = item.getType().getMaxDurability();
                int damage = actualDamage*maxDurability/actualMaxDurability;
                nms.setDamageValue(actualDamage*maxDurability/actualMaxDurability);
                if (damage>=maxDurability){
                    damageEvent.setCancelled(false);
                }
                List<String> lore = item.getLore();
                List<String> newLore = new ArrayList<String>();
                for (int i=0;i<lore.size();i++){
                    newLore.add(i!=loreLineIndex ? lore.get(i) : "Durability: "+Integer.toString(actualMaxDurability-actualDamage)+" / "+Integer.toString(actualMaxDurability));
                }
                ItemStack newItem = CraftItemStack.asBukkitCopy(nms);
                item.setItemMeta(newItem.getItemMeta());
                item.setLore(newLore);

                
            }
        }


    }

    @EventHandler
    public void onMendingRepair(PlayerItemMendEvent mendingEvent){
        ItemStack item = mendingEvent.getItem();
        net.minecraft.world.item.ItemStack nms = CraftItemStack.asNMSCopy(item);
        if (nms.hasTag()){
            CompoundTag nbt = nms.getTag();
            if (nbt.contains("actual-maxdurability")){
                mendingEvent.setCancelled(true);
                if (nbt.contains("multicolor")){
                    MulticolorArmor.fromMulticolorArmorItem(item);
                } 
                short maxDurability = nbt.getShort("actual-maxdurability");
                int actualDamage = nbt.getInt("actual-damage");
                int loreLineIndex = nbt.getInt("lore-durability-line");
                actualDamage -= mendingEvent.getRepairAmount();
                nbt.putInt("actual-damage", actualDamage);
                nms.setTag(nbt);
                nms.setDamageValue(actualDamage*item.getType().getMaxDurability()/maxDurability);
                List<String> lore = item.getLore();
                List<String> newLore = new ArrayList<String>();
                for (int i=0;i<lore.size();i++){
                    newLore.add(i!=loreLineIndex ? lore.get(i) : "Durability: "+Integer.toString(maxDurability-actualDamage)+" / "+Integer.toString(maxDurability));
                }
                ItemStack newItem = CraftItemStack.asBukkitCopy(nms);
                item.setItemMeta(newItem.getItemMeta());
                item.setLore(newLore);

            }
        }


    }

    @EventHandler
    public void onPlayerSignWrite(SignChangeEvent sign){
        int lineNb = 0;
        for (String line:sign.getLines()){
            sign.setLine(lineNb, ColorTranslator.translateAlternateColorCodes(line));
            lineNb += 1;
        }

    }

    public void refund(Player dropper,List<Entity> entitiesCleared){
        for (Entity entity:entitiesCleared){
            if (entity instanceof Item){
                Item item = (Item)entity;
                ItemStack s = item.getItemStack();
                if (hasAvailableSlot(dropper.getInventory())){
                    dropper.getInventory().addItem(s);
                } else {
                    Bukkit.getServer().getScheduler().runTaskLater(MoonCore.getInstance(), ()->{dropper.getWorld().dropItemNaturally(dropper.getLocation(), s);},0);
                    
                }
            }     
        }


    }

    public boolean hasAvailableSlot(org.bukkit.inventory.PlayerInventory inventory) {
        for (ItemStack item: inventory.getStorageContents()) {
                if(item == null) {
                        return true;
                }
            }
    return false;
    }

    @EventHandler
    public void onPlayerItemDrop(PlayerDropItemEvent droppeditem){


                Material droppedMaterial = droppeditem.getItemDrop().getItemStack().getType();
                if (droppedMaterial!=Material.MAP && droppedMaterial!=Material.WRITTEN_BOOK){
                    ItemStack droppedItem = droppeditem.getItemDrop().getItemStack();
                    if (MulticolorArmor.isMulticolorArmorItem(droppedItem)){
                        MulticolorArmor piece = MulticolorArmor.fromMulticolorArmorItem(droppedItem);
                        piece.start();
                    }
                    return;
                }
                Player dropper = droppeditem.getPlayer();
                List<Entity> col = new ArrayList<Entity>();
                PictureCreatingQueue queue = PictureCreatingQueue.fromPlayer(dropper);
                // Long now = System.currentTimeMillis();
                queue.lastRequest = droppeditem;
                // if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("New request: "+Long.toString(now)+" ("+droppeditem.toString()+") from queue: "+queue.toString()+" of "+queue.player.getName());
                threadPool.execute(()->{
                    Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
                    synchronized (queue){ //This ensures that a player cannot make a request while another one is ongoing
                        // if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("Request "+Long.toString(now)+" now executing...");
                        if (queue.lastRequest!=droppeditem){return;}//make sure only one request can be queued at a time, the most recent one 
                        //if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("Request "+Long.toString(now)+" cancelled, because more recent one "+queue.lastRequest.toString());

                        synchronized (col){//We retrieve every entities on the floor "right now" (since it might have been updated since request was done, due to sync)
                        // if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("Request "+Long.toString(now)+" now scheduling to retrieve items...");
                            Bukkit.getScheduler().runTaskLater(MoonCore.getInstance(), ()->{
                                // if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("Request "+Long.toString(now)+" in item retrievr scheduler");
                                //The API doesn't allow async call to get nearby entities
                                synchronized (col){
                                    // if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("Request "+Long.toString(now)+" retrieving now items");
                                    if (queue.lastRequest!=droppeditem){col.notify();}//if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("Request "+Long.toString(now)+" cancelled, because more recent one "+queue.lastRequest.toString());
                                    else{
                                        col.addAll(dropper.getNearbyEntities(5, 5, 5));
                                        col.notify();
                                    }
                                }
                                
                            }, 1);
                            try{
                                // if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("Request "+Long.toString(now)+" now waiting for scheduler to answer");
                                col.wait();//We await on the thread for the entities to be added by the sync task
                            } catch (Exception e){e.printStackTrace();return;}
                            
                        }

                        if (col.isEmpty()) return;
                        
                        // if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("Request "+Long.toString(now)+" now answered: "+col.size());
                        // String dbg = "";
                        // for (Entity ent:col) {
                        //     if (ent instanceof Item) dbg+=ent.getName()+" ("+Integer.toString(((Item)ent).getItemStack().getAmount())+"), ";
                        // }
                        // if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("Request "+Long.toString(now)+" items: "+dbg);
                        
                        int toRefund = 0;
                    
                        List<Material> items = new ArrayList<>();
                        List<Entity> entitiesToClear = new ArrayList<>();
                        
                        ItemStack book=null;
                        // ItemStack emptyMap = null;
                        int emptyMapAmount = 0;
                        for (Entity entity:col){
                            if (entity instanceof Item){
                                Item item = (Item)entity;
                                ItemStack s = item.getItemStack();
                                Material material = s.getType();
                                if ((material==Material.MAP || material==Material.WRITTEN_BOOK) && !(items.contains(material))){
                                    items.add(material);
                    
                                
                                if (material==Material.WRITTEN_BOOK){
                                    book = s;
                                    entitiesToClear.add(entity);
                                }
                                if (material==Material.MAP){
                                    // emptyMap = s;
                                    emptyMapAmount = s.getAmount();
                                    entitiesToClear.add(entity);
                                }
                            } else if (material==Material.MAP){
                                emptyMapAmount += s.getAmount();
                                entitiesToClear.add(entity);
                            }
                        }
        
                        }
        
        
                        if (items.size()!=2){
                            return;
                        }

                        try{Thread.sleep(50);} catch (Exception e){}
                        // if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("Request "+Long.toString(now)+" empty maps amount detected: "+Integer.toString(emptyMapAmount));
                        dropper.sendMessage(ColorTranslator.translateAlternateColorCodes("<#00bdff>Retrieving the image(s) for you..."));
                        BookMeta meta = (BookMeta) book.getItemMeta();
                        String urlstring = meta.getPage(1).replaceAll("§", "&");
                        String title = meta.getTitle();
                        String properties = (meta.getPages().size()>1) ? meta.getPage(2) : "adaptive";
                        String disposal = properties.toLowerCase().trim();
        
                        ImageLoader image;
                        int width;
                        int height;
                        boolean dither = disposal.contains("dither");
                        try {
                            image = new ImageLoader(urlstring,disposal.contains("animated"));
                            // if (image.isAnimated){
                            //     if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("it is animated");
                            // } else{
                            //     if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("it ain't animated");
                            // }
                            width = image.main.getWidth(null);
                            height = image.main.getHeight(null);
                        } catch (NullPointerException e){
                            dropper.sendMessage("§cCouldn't find the image at the given source: make sure it directly leads to the image (and not to a webpage containing the image) and that it is accessible at this URL.");return;
                        } catch (FileNotFoundException e){
                            dropper.sendMessage("§cThe link you provided doesn't exist.");return;
                        }
                        catch (IOException e) {
                            dropper.sendMessage("§c403: The source doesn't allow download.");return;
                        } 

                        try{Thread.sleep(50);} catch (Exception e){}
                            
        
                        List<Picture> images = new ArrayList<Picture>();
                        Image newFrame;
                        BufferedImage bimage;
                        Graphics2D bGr;
                        int decalX = 0;
                        int decalY = 0;
                        int mapsNeededAmount = 1;
                        int maxMapsGivable = 150;
                        int proportionX = 1; int proportionY = 1;
                        if (disposal.startsWith("adaptive") || disposal.isEmpty()){
                            // if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("Request "+Long.toString(now)+" empty maps amount detected: "+Integer.toString(emptyMapAmount));
                            Matcher proportionsMatcher = Pattern.compile(" (\\d+)x(\\d+)")
                            .matcher(disposal);
        
                            try {
                                
                                if (proportionsMatcher.find()){
                                    proportionX = Integer.parseInt(proportionsMatcher.group(1));
                                    proportionY = Integer.parseInt(proportionsMatcher.group(2));
                                }
                                mapsNeededAmount = proportionX*proportionY;
                                if (proportionX<1 || proportionY<1){
                                    dropper.sendMessage("§cThe proportions indicated in your property aren't accepted. Accepted values are integer numbers bigger or equal to 1. Example: adaptive 3x2");return;
                                }
                            } catch (Exception e) {
                                dropper.sendMessage("§cThe proportions indicated in your property aren't accepted. Accepted values are integer numbers bigger or equal to 1. Example: adaptive 3x2");return;
                            }
        
                            if (mapsNeededAmount>maxMapsGivable){
                                dropper.sendMessage("§cThe image you are requesting requires above "+ maxMapsGivable +" maps. Please reduce the proportions.");return;
                            }
        
                            if (emptyMapAmount<mapsNeededAmount){
                                // if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("Request "+Long.toString(now)+" empty maps amount detected: "+Integer.toString(emptyMapAmount)+"total needed: "+Integer.toString(mapsNeededAmount)+" Send "+ (mapsNeededAmount - emptyMapAmount) +" more empty maps for this image.");
                                dropper.sendMessage("§cSend "+ (mapsNeededAmount - emptyMapAmount) +" more empty maps for this image.");return;
                            }
                            if (emptyMapAmount>mapsNeededAmount){
                                toRefund = emptyMapAmount-mapsNeededAmount;
                            }
                            try{Thread.sleep(50);} catch (Exception e){}
                            try{
                                Bukkit.getScheduler().runTask(MoonCore.getInstance(),()->{
                                    for (Entity entity:entitiesToClear){
                                        entity.remove();
                                    }
                                });
                                queue.initializeProgressBar(dropper);
                                image.prepareAnimatedPic();
                            } catch (Exception e){
                                dropper.sendMessage("§cAn error happened while preparing the animated image; see console.");e.printStackTrace();queue.finish();refund(dropper,entitiesToClear);return;
                            }
                            
                            int availableSpaceX = 128*proportionX;
                            int availableSpaceY = 128*proportionY;

                            // int squareLength = 0;
                            // if (width>=height){
                            //     squareLength = width/proportionX;
                            //     decalY = (height%squareLength)/2;
                            // } else {
                            //     squareLength = height/proportionY;
                            //     decalX = (width%)
                            // }
        
        
                            if (width>height){ // The following if-else block is fundamentally useless, the code can technically work with only one block: but those are present to run the operations in a more logical order to reduce the approximations after deviding and keeping as much as possible the proportions of the image.
                                if (width<availableSpaceX && height<availableSpaceY){
                                    height=height*(availableSpaceX/width);width=availableSpaceX;
                                } else if (width>availableSpaceX){
                                    height=height/(width/availableSpaceX);width=availableSpaceX;
                                } if (height>availableSpaceY){
                                    width=width/(height/availableSpaceY);height=availableSpaceY;
                                }
        
                            } else{
                                if (width<availableSpaceX && height<availableSpaceY){
                                    width=width*(availableSpaceY/height);height=availableSpaceY;
                                } else if (height>availableSpaceY){
                                    width = width/(height/availableSpaceY);height=availableSpaceY;
                                } if (width>availableSpaceX){
                                    height=height/(width/availableSpaceX);width=availableSpaceX;
                                }
                            }
        
                            if (height==availableSpaceY){
                                decalX=(availableSpaceX-width)/2;
                            } else {
                                decalY=(availableSpaceY-height)/2;
                            }
                            try{Thread.sleep(50);} catch (Exception e){}
                            
                            
                            // if (image.isAnimated) image.saveFile("temp/ABC_002_1906_adaptive_newImage","temp","temp");
                            images = image.createSubPictureTemplates(dropper, mapsNeededAmount);
                            try{Thread.sleep(50);} catch (Exception e){}
                            while (image.hasNextFrame()){
                                try{
                                    newFrame = image.nextFrame().getScaledInstance(width,height, Image.SCALE_SMOOTH);
                                } catch (ArrayIndexOutOfBoundsException ar){
                                    System.out.println("arrayoutofbound while generating img at frame "+Integer.toString(image.currentIndex)+" / "+Integer.toString(image.frameAmount)+" (actualindex="+Integer.toString((image.currentIndex+image.minIndex)%image.frameAmount)+")");image.currentIndex+=1;ar.printStackTrace();continue;
                                }  catch (Exception e){
                                    dropper.sendMessage("§cAn error happened while retrieving a/the frame of the image; see console.");e.printStackTrace();queue.finish();refund(dropper,entitiesToClear);return;
                                }
                                bimage = new BufferedImage(availableSpaceX, availableSpaceY, BufferedImage.TYPE_INT_ARGB);
                                bGr = bimage.createGraphics();
                                bGr.drawImage(newFrame, decalX, decalY, null);
                                bGr.dispose();
                                if (dither) Dithering.applyFloydSteinbergDithering(bimage);
                                int i = 0;

                                for (int y=0;y<availableSpaceY;y+=128){
                                    for (int x=0;x<availableSpaceX;x+=128){
                                        try{Thread.sleep(8);} catch (Exception e){}
                                        queue.updateProgressBar(image.currentIndex-1, image.frameAmount,i, mapsNeededAmount);
                                        images.get(i).addFrame(bimage.getSubimage(x,y,128,128),image.lastDelay);
                                        i++;
                                    }
                                }

                            }
                            
                        } else if (disposal.startsWith("fill")){
                            Matcher proportionsMatcher = Pattern.compile(" (\\d+)x(\\d+)")
                            .matcher(disposal);
                            
                            try {
                                
                                if (proportionsMatcher.find()){
                                    proportionX = Integer.parseInt(proportionsMatcher.group(1));
                                    proportionY = Integer.parseInt(proportionsMatcher.group(2));
                                }
                                mapsNeededAmount = proportionX*proportionY;
                                if (proportionX<1 || proportionY<1){
                                    dropper.sendMessage("§cThe proportions indicated in your property aren't accepted. Accepted values are integer numbers bigger or equal to 1. Example: fill 3x2");return;
                                }
                            } catch (Exception e) {
                                dropper.sendMessage("§cThe proportions indicated in your property aren't accepted. Accepted values are integer numbers bigger or equal to 1. Example: fill 3x2");return;
                            }
        
                            if (mapsNeededAmount>maxMapsGivable){
                                dropper.sendMessage("§cThe image you are requesting requires above "+ maxMapsGivable +" maps. Please reduce the proportions.");return;
                            }
        
                            if (emptyMapAmount<mapsNeededAmount){
                                dropper.sendMessage("§cSend "+ (mapsNeededAmount - emptyMapAmount) +" more empty maps for this image.");return;
                            }
                            if (emptyMapAmount>mapsNeededAmount){
                                toRefund = emptyMapAmount-mapsNeededAmount;
                            }
                            try{Thread.sleep(50);} catch (Exception e){}
                            try{
                                Bukkit.getScheduler().runTask(MoonCore.getInstance(),()->{
                                    for (Entity entity:entitiesToClear){
                                        entity.remove();
                                    }
                                });
                                queue.initializeProgressBar(dropper);
                                image.prepareAnimatedPic();
                            } catch (Exception e){
                                dropper.sendMessage("§cAn error happened while preparing the animated image; see console.");e.printStackTrace();queue.finish();refund(dropper,entitiesToClear);return;
                            }
        
                            width = 128*proportionX;
                            height = 128*proportionY;

                            try{Thread.sleep(50);} catch (Exception e){}
        
                            images = image.createSubPictureTemplates(dropper, mapsNeededAmount);
                            try{Thread.sleep(50);} catch (Exception e){}
                            while (image.hasNextFrame()){
                                try{
                                    newFrame = image.nextFrame().getScaledInstance(width,height, Image.SCALE_SMOOTH);
                                } catch (ArrayIndexOutOfBoundsException ar){
                                    System.out.println("arrayoutofbound while generating img at frame "+Integer.toString(image.currentIndex)+" / "+Integer.toString(image.frameAmount)+" (actualindex="+Integer.toString((image.currentIndex+image.minIndex)%image.frameAmount)+")");image.currentIndex+=1;ar.printStackTrace();continue;
                                } catch (Exception e){
                                    dropper.sendMessage("§cAn error happened while retrieving a/the frame of the image; see console.");e.printStackTrace();queue.finish();refund(dropper,entitiesToClear);return;
                                }
                                
                                bimage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
                                bGr = bimage.createGraphics();
                                bGr.drawImage(newFrame, 0,0, null);
                                bGr.dispose();
                                if (dither) Dithering.applyFloydSteinbergDithering(bimage);
                                int i = 0;

                                for (int y=0;y<height;y+=128){
                                    for (int x=0;x<width;x+=128){
                                        try{Thread.sleep(8);} catch (Exception e){}
                                        queue.updateProgressBar(image.currentIndex-1, image.frameAmount,i, mapsNeededAmount);
                                        images.get(i).addFrame(bimage.getSubimage(x,y,128,128),image.lastDelay);
                                        i++;
                                    }
                                }

                            }

                        } else if (disposal.startsWith("expand")){
                            int zoom;
                            Matcher sizeMatcher = Pattern.compile(" x(\\S+)")
                                .matcher(disposal);
                            try {
                                zoom = (sizeMatcher.find())? Integer.parseInt(sizeMatcher.group(1)) : 1;
                                if (zoom<0){
                                    dropper.sendMessage("§cThe zoom factor indicated in your property isn't accepted. Accepted values are integer numbers bigger or equal to 1. Example: expand x2");return;
                                }
                            } catch (Exception e) {
                                dropper.sendMessage("§cThe zoom factor indicated in your property isn't accepted. Accepted values are integer numbers bigger or equal to 1. Example: expand x2");return;
                            }
                            int minSize = 128*zoom;
                            int actualwidth;
                            int actualheight;
                            if (width > height) {
                                width = (width * minSize) / height;
                                actualwidth = (width % 128 == 0) ? width : (width / 128 + 1) * 128;
                                height = minSize;
                                actualheight = height;
        
                            }
                            else if (height > width){
                                height = (height * minSize) / width;
                                actualheight = (height % 128 == 0) ? height : (height / 128 + 1) * 128;
                                width = minSize; actualwidth = width;
                            } else {
                                height = minSize; width = minSize; actualheight = minSize; actualwidth = minSize;
                            }
                            proportionX = actualwidth/128;
                            proportionY = actualheight/128;
                            mapsNeededAmount = proportionX*proportionY;
                            if (mapsNeededAmount>maxMapsGivable){
                                String maxMapsString = Integer.toString(maxMapsGivable);
                                dropper.sendMessage("§cThe images you are requesting require above "+ maxMapsString+" maps. The limit is set to "+ maxMapsString +": either reduce the zoom level on second page of the book, or pick an image which width vs height is less.");return;
                            }
                            if (emptyMapAmount<mapsNeededAmount){
                                dropper.sendMessage("§cSend " + (mapsNeededAmount - emptyMapAmount) + " more empty maps for this image.");return;
                            }
                            if (emptyMapAmount>mapsNeededAmount){
                                toRefund = emptyMapAmount - mapsNeededAmount;
                            }
                            decalX = (actualwidth-width)/2;
                            decalY = (actualheight-height)/2;

                            try{Thread.sleep(50);} catch (Exception e){}
                            try{
                                Bukkit.getScheduler().runTask(MoonCore.getInstance(),()->{
                                    for (Entity entity:entitiesToClear){
                                        entity.remove();
                                    }
                                });
                                queue.initializeProgressBar(dropper);
                                image.prepareAnimatedPic();
                            } catch (Exception e){
                                dropper.sendMessage("§cAn error happened while preparing the animated image; see console.");e.printStackTrace();queue.finish();refund(dropper,entitiesToClear);return;
                            }
                            try{Thread.sleep(50);} catch (Exception e){}
                            images = image.createSubPictureTemplates(dropper, mapsNeededAmount);
                            try{Thread.sleep(50);} catch (Exception e){}
                            while (image.hasNextFrame()){
                                try{
                                    newFrame = image.nextFrame().getScaledInstance(width,height, Image.SCALE_SMOOTH);
                                } catch (ArrayIndexOutOfBoundsException ar){
                                    System.out.println("arrayoutofbound while generating img at frame "+Integer.toString(image.currentIndex)+" / "+Integer.toString(image.frameAmount)+" (actualindex="+Integer.toString((image.currentIndex+image.minIndex)%image.frameAmount)+")");image.currentIndex+=1;ar.printStackTrace();continue;
                                }  catch (Exception e){
                                    dropper.sendMessage("§cAn error happened while retrieving a/the frame of the image; see console.");e.printStackTrace();queue.finish();refund(dropper,entitiesToClear);return;
                                }
                                
                                bimage = new BufferedImage(actualwidth, actualheight, BufferedImage.TYPE_INT_ARGB);
                                bGr = bimage.createGraphics();
                                bGr.drawImage(newFrame, decalX, decalY, null);
                                bGr.dispose();
                                if (dither) Dithering.applyFloydSteinbergDithering(bimage);
                                int i = 0;

                                for (int y=0;y<height;y+=128){
                                    for (int x=0;x<width;x+=128){
                                        try{Thread.sleep(8);} catch (Exception e){}
                                        queue.updateProgressBar(image.currentIndex-1, image.frameAmount,i, mapsNeededAmount);
                                        images.get(i).addFrame(bimage.getSubimage(x,y,128,128),image.lastDelay);
                                        i++;
                                    }
                                }

                            }
        
                        } else {
                            dropper.sendMessage("§cDisposal type on page 2 is invalid. Accepted types: adaptive, fill or expand.");
                            return;
                        }

                        try{Thread.sleep(50);} catch (Exception e){}
                        int x=0; int y=0;
                        String coordinates;
                        queue.setFinalStatus();
                        for (Picture img:images){
                            try{Thread.sleep(50);} catch (Exception e){}
                            img.write();
                            try{Thread.sleep(50);} catch (Exception e){}
                            img.startRendering(); 
                            
        
                            
                            coordinates = x +", "+ y;


                            
                            ItemStack toGive = new ItemStack(Material.FILLED_MAP);
                            MapMeta toGiveMeta = (MapMeta) toGive.getItemMeta();
                            List<String> lore = new ArrayList<>();
                            lore.add(title);
                            lore.add("§5Coordinates: ("+coordinates+")");
                            String genericDot = ColorTranslator.translateAlternateColorCodes("<#4842f9>⬛");
                            String theDot = ColorTranslator.translateAlternateColorCodes("<#ff6114>&l⬛");
                            String line;
                            for (int Y=0; Y<proportionY;Y++){
                                line = "";
                                for (int X=0;X<proportionX;X++){
                                    line += ((X==x && Y==y) ? theDot : genericDot);
                                }
                                lore.add(line);

                            }
                            x += 1;
                            if (x>=proportionX){
                                y += 1;
                                x = 0;
                            }
                            try{Thread.sleep(50);} catch (Exception e){}
                            if (image.frameAmount>1){
                                lore.add(ColorTranslator.translateAlternateColorCodes("&6&k&l!! &5a&1n&2i&em&6a&ct&4e&dd &6&k&l!!"));
                            }
                            
                            toGiveMeta.setLore(lore);
                            toGiveMeta.setMapView(img.map);
                            toGive.setItemMeta(toGiveMeta);
                            
                            if (hasAvailableSlot(dropper.getInventory())){
                                dropper.getInventory().addItem(toGive);
                            } else {
                                Bukkit.getServer().getScheduler().runTaskLater(MoonCore.getInstance(), ()->{dropper.getWorld().dropItemNaturally(dropper.getLocation(), toGive);},0);
                                
                            }
        
                        }
                        if (toRefund>0){
                            ItemStack emptyMap2 = new ItemStack(Material.MAP);
                            emptyMap2.setAmount(toRefund);
                            Bukkit.getServer().getScheduler().runTaskLater(MoonCore.getInstance(), ()->{dropper.getWorld().dropItemNaturally(dropper.getLocation(),emptyMap2);},0);
                            
                        }
                        queue.finish();
                        dropper.sendMessage("§2The pictures have been given to you.");
                        // if (AnimatedPicGroupHolders.debuglevel1()) System.out.println("Request "+Long.toString(now)+"is now over");
                    };
                });
                // Bukkit.getServer().getScheduler().scheduleAsyncDelayedTask(mooncore.getInstance(), ()->{}, 0);



           
    }

    @EventHandler
    public void onPickup(EntityPickupItemEvent event){
        Item actualItem = event.getItem();
        ItemStack item = actualItem.getItemStack();
        Entity toremove = ((Entity)actualItem);
        Entity entity = event.getEntity();
        if (MulticolorArmor.isMulticolorArmorItem(item)){
            
            toremove.remove();
            MulticolorArmor piece = MulticolorArmor.fromMulticolorArmorItem(item);
            if (entity instanceof Player){
                event.setCancelled(true);
                Player player = (Player)entity;
                piece.addToInventory(player.getInventory());
            }
            
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerClickItemFram(PlayerInteractEntityEvent event) {
        if (event.getRightClicked() instanceof ItemFrame){
            ItemFrame itemframe = (ItemFrame)event.getRightClicked();
            Player player = event.getPlayer();
            Material inHand = event.getPlayer().getInventory().getItemInMainHand().getType();
            ItemStack itemHeld = itemframe.getItem();
            if (itemHeld.getType()!=Material.AIR){
                if (inHand==Material.GLASS_PANE){
                    event.setCancelled(true);
                    itemframe.setVisible(!(itemframe.isVisible()));
                    player.getWorld().spawnParticle(Particle.PORTAL,itemframe.getLocation(),30);
                } if (inHand==Material.HONEYCOMB){
                    event.setCancelled(true);
                    if (itemframe.isFixed()){
                        itemframe.setFixed(false);
                        player.sendMessage("§bNow §aunfixed§b!"); 

                    } else {
                        itemframe.setFixed(true);
                        player.sendMessage(ColorTranslator.translateAlternateColorCodes("&bNow &cfixed&b! Use &#fbde2aHONEYCOMB &bagain to unfix, or &#d5d5d5SHEARS &bto drop it."));
                    }
                    
                    player.getWorld().spawnParticle(Particle.WAX_OFF,itemframe.getLocation(),20);
                } if (inHand==Material.SHEARS){
                    event.setCancelled(true);
                    // net.minecraft.world.entity.decoration.ItemFrame nmsItemframeEntity = ((CraftItemFrame)itemframe).getHandle();
                    net.minecraft.world.item.ItemStack nmsItemframeItem = CraftItemStack.asNMSCopy(new ItemStack((event.getRightClicked() instanceof GlowItemFrame) ? Material.GLOW_ITEM_FRAME : Material.ITEM_FRAME));
                    // nmsItemframeItem.setEntityRepresentation(nmsItemframeEntity);
                    CompoundTag nbt = new CompoundTag();
                    CompoundTag tag = new CompoundTag();
                    tag.putBoolean("Invisible",!itemframe.isVisible());
                    tag.putBoolean("Fixed",itemframe.isFixed());
                    tag.putInt("ItemRotation",itemframe.getRotation().ordinal()); 
                    CompoundTag itemHeldTag = new CompoundTag();
                    itemHeldTag.putString("id",itemHeld.getType().key().asString());
                    itemHeldTag.put("tag",CraftItemStack.asNMSCopy(itemHeld).getOrCreateTag());
                    itemHeldTag.putInt("Count",itemHeld.getAmount());
                    tag.put("Item",itemHeldTag);
                    nbt.put("EntityTag",tag);
                    nmsItemframeItem.setTag(nbt);
                    
                    ItemStack ItemframeItem = CraftItemStack.asBukkitCopy(nmsItemframeItem);
                    List<String> newLore = new ArrayList<String>();
                    
                    String name =itemHeld.getDisplayName();
                    List<String> lore = itemHeld.getLore();
                    newLore.add("§6Invisible:§b "+Boolean.toString((!itemframe.isVisible())));
                    newLore.add("§6Fixed §o(can hang on nothing):§b "+Boolean.toString(itemframe.isFixed()));
                    newLore.add("§6Rotation §o(of the item inside):§b "+switch(itemframe.getRotation()){
                        case NONE  -> "⬆";
                        case CLOCKWISE_45 -> "⬈";
                        case CLOCKWISE -> "➡";
                        case CLOCKWISE_135 -> "⬊";
                        case FLIPPED -> "⬇";
                        case FLIPPED_45 -> "⬋";
                        case COUNTER_CLOCKWISE -> "⬅";
                        case COUNTER_CLOCKWISE_45 -> "⬉";
                    });
                    newLore.add("§6Item: §b "+itemHeld.getType().name()+(name.isEmpty() ? "" : " §b("+name+"§b)"));
                    if (lore!=null){
                        for (int i=0;i<lore.size();i++){
                            newLore.add(lore.get(i));
                        }
                    }
                    ItemframeItem.setLore(newLore);
                    itemframe.remove();
                    player.getWorld().dropItemNaturally(player.getLocation(), ItemframeItem);

                }
            } else {
                if (inHand==Material.GLASS_PANE){
                    itemframe.setVisible(false);
                    player.getWorld().spawnParticle(Particle.PORTAL,itemframe.getLocation(),30);
                }
            }
        } 
        // else if (event.getRightClicked() instanceof ArmorStand){
        //     ArmorStand armorstand = ((ArmorStand)event.getRightClicked());
        //     armorstand.
        // }
    }



}
