package moonlit.alexis.mooncore.listener;

import com.vdurmont.emoji.EmojiParser;
import org.purpurmc.purpur.event.PlayerAFKEvent;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerAdvancementDoneEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.BroadcastMessageEvent;
import moonlit.alexis.mooncore.MoonCore;
import moonlit.alexis.mooncore.configuration.Config;
import moonlit.alexis.mooncore.configuration.Lang;
import moonlit.alexis.mooncore.util.ColorTranslator;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.TranslatableComponent;
import org.bukkit.entity.Player;
import net.kyori.adventure.text.serializer.plain.PlainTextComponentSerializer;
import io.papermc.paper.text.PaperComponents;
import org.bukkit.Bukkit;
import org.apache.commons.lang3.StringUtils;

public class PurpurListener implements Listener {
    private final MoonCore plugin;
    private final PlainTextComponentSerializer serializer = PaperComponents.plainTextSerializer();

    public PurpurListener(MoonCore plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        event.setJoinMessage(null);
        event.getPlayer().sendMessage(StringUtils.repeat(" \n", 99));
        Bukkit.getServer().getScheduler().runTaskLater(MoonCore.getInstance(), ()->{
            Player player = event.getPlayer();
            String prefix = MoonCore.getInstance().getPlayerPrefix(player);
            String displayName = player.getDisplayName();
            String mc_msg = Lang.MINECRAFT_PLAYERJOIN_MESSAGE
            .replace("{playerName}",displayName)
            .replace("{serverName}",Config.SERVER_NAME)
            .replace("{playerPrefix}",prefix)
            .replace("{playerSuffix}",MoonCore.getInstance().getPlayerSuffix(player))
            .replace("{playerColor}",ColorTranslator.extractFormats(prefix+displayName));
            mc_msg = ColorTranslator.translateAlternateColorCodes(mc_msg);
            Lang.sendToAllPlayers(mc_msg,false,false);
            if (plugin.getBot().broadcastChannel==null) Lang.sendToAllProxyPlayers(mc_msg,player,false,false);
            else plugin.getBot().sendToDiscordBroadcast(mc_msg);
            plugin.getBot().sendMessageToDiscord(Lang.DISCORD_PLAYERJOIN_MESSAGE.replace("{playerName}",player.getName()));
        },5);

    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        String prefix = MoonCore.getInstance().getPlayerPrefix(player);
        String displayName = player.getDisplayName();
        String mc_msg = Lang.MINECRAFT_PLAYERLEAVE_MESSAGE.replace("{playerName}",displayName)
        .replace("{serverName}",Config.SERVER_NAME)
        .replace("{playerPrefix}",prefix)
        .replace("{playerSuffix}",MoonCore.getInstance().getPlayerSuffix(player))
        .replace("{playerColor}",ColorTranslator.extractFormats(prefix+displayName));
        mc_msg = ColorTranslator.translateAlternateColorCodes(mc_msg);
        event.setQuitMessage(mc_msg);
        if (plugin.getBot().broadcastChannel==null) Lang.sendToAllProxyPlayers(mc_msg,null,false,false);
        else plugin.getBot().sendToDiscordBroadcast(mc_msg);
        
        plugin.getBot().sendMessageToDiscord(Lang.DISCORD_PLAYERLEAVE_MESSAGE.replace("{playerName}",player.getName()));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerDeath(PlayerDeathEvent event) {
        String msg = event.getDeathMessage();
        if (msg == null || msg.isEmpty() || msg.equalsIgnoreCase("null")) {
            return;
        }
        plugin.getBot().sendMessageToDiscord(Lang.DISCORD_PLAYERDEATH_MESSAGE
        .replace("{playerName}",event.getPlayer().getDisplayName())
        .replace("{message}",msg));
        Lang.sendToAllProxyPlayers(msg);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onAdvancement(PlayerAdvancementDoneEvent event) {
        io.papermc.paper.advancement.AdvancementDisplay display = event.getAdvancement().getDisplay();
        this.plugin.displaytemp = display;
        if (display == null) {
            return; // nothing to display
        }

        if (!display.doesAnnounceToChat()) {
            return; // do not display
        }

        if (!event.getPlayer().getWorld().isGameRule("announceAdvancements")) {
            return; // do not display
        }


        io.papermc.paper.advancement.AdvancementDisplay.Frame frame = display.frame();
        String icon;

        switch (frame) {
            case CHALLENGE:
                icon = Lang.ADVANCEMENT_ICON_CHALLENGE;
                break;
            case GOAL:
                icon = Lang.ADVANCEMENT_ICON_GOAL;
                break;
            case TASK:
            default:
                icon = Lang.ADVANCEMENT_ICON_TASK;
        }

        Player player = event.getPlayer();
        String title = serializer.serialize(display.title());
        String displayName = player.getDisplayName();
        String prefix = MoonCore.getInstance().getPlayerPrefix(player);
        Lang.sendToAllProxyPlayers(Lang.MINECRAFT_ADVANCEMENT_FORMAT
        .replace("{playerPrefix}",prefix)
        .replace("{playerName}",displayName)
        .replace("{playerSuffix}",MoonCore.getInstance().getPlayerSuffix(player))
        .replace("{title}",title)
        .replace("{playerColor}",ColorTranslator.extractFormats(prefix+displayName)));

        plugin.getBot().sendMessageToDiscord(ChatColor.stripColor(Lang.colorize(Lang.DISCORD_ADVANCEMENT_FORMAT
                .replace("{icon}", icon)
                .replace("{playerName}", player.getName())
                .replace("{type}", frame.toString().toLowerCase())
                .replace("{title}", title)
                .replace("{description}", serializer.serialize(display.description())))));
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        String message = EmojiParser.parseToUnicode(event.getMessage()
                .replace(":)", ":smiley:")
                .replace("(:", ":smiley:")
                .replace(":P", ":stuck_out_tongue:")
                .replace(":p", ":stuck_out_tongue:")
                .replace(":(", ":frowning:")
                .replace(":c", ":frowning:")
                .replace(":C", ":frowning:")
                .replace("D:", ":frowning:")
                .replace(":D", ":smile:")
                .replace("xD", ":smile:")
                .replace("XD", ":smile:")
                .replace(":'(", ":cry:")
        );

        plugin.getBot().sendMessageToDiscord(event.getPlayer(), message);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBroadcast(BroadcastMessageEvent event) {
        String msg = event.getMessage();
        if (msg == null || msg.isEmpty() || msg.equalsIgnoreCase("null")) {
            return;
        }
        plugin.getBot().sendMessageToDiscord(Lang.BROADCAST_FORMAT.replace("{message}",msg));
        Lang.sendToAllProxyPlayers(msg);
    }
}