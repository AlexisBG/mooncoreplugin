package moonlit.alexis.mooncore;

import moonlit.alexis.mooncore.bot.Bot;
import moonlit.alexis.mooncore.commands.*;
import moonlit.alexis.mooncore.configuration.Config;
import moonlit.alexis.mooncore.configuration.Data;
import moonlit.alexis.mooncore.configuration.Lang;
import moonlit.alexis.mooncore.listener.*;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.entity.Player;
import net.luckperms.api.model.user.User;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.LuckPerms;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bukkit.plugin.messaging.PluginMessageListener;

import com.google.common.io.ByteStreams;
import com.google.common.io.ByteArrayDataInput;
// import com.google.common.collect.Iterables;

// import com.google.common.io.ByteStreams;
// import com.google.common.io.ByteArrayDataOutput;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.ByteArrayInputStream;

public class MoonCore extends JavaPlugin implements PluginMessageListener{

    private static MoonCore instance;
    private final Bot bot;
    private ConsoleAppender consoleAppender;
    public LuckPerms luckPerms; 
    ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
    public io.papermc.paper.advancement.AdvancementDisplay displaytemp;
    String command = "stop";
    int i = 600;

    public MoonCore() {
        super();
        instance = this;
        bot = new Bot(this);
    }

    @Override
    public void onEnable() {

        consoleAppender = new ConsoleAppender(this);
        Config.reload(this);
        Lang.reload(this);
        Data.getInstance();
        try{
            bot.connect();
        } catch (Exception e){
            e.printStackTrace();
        }
        if (!getServer().getPluginManager().isPluginEnabled("LuckPerms")) {
            Logger.error("Dependency plugin LuckPerms not found!");
            Logger.error("Disabling " + getName());
            return;
        }
        try{
            luckPerms = LuckPermsProvider.get();
        } catch (Exception e){
            e.printStackTrace();
        }
        
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        this.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", this);



        // if (!getServer().getPluginManager().isPluginEnabled("LiteBans")) {
        //     Logger.error("Dependency plugin LiteBans not found!");
        //     Logger.error("Disabling " + getName());
        //     return;
        // }

        // if (!getServer().getPluginManager().isPluginEnabled("MoonClaims")) {
        //     Logger.error("Dependency plugin MoonClaims not found!");
        //     Logger.error("Disabling " + getName());
        //     return;
        // }

        // Found dependency's
        // if (getServer().getPluginManager().isPluginEnabled("LiteBans")) {
        //     Logger.info("LiteBans found. Initiating hook...");
        // }

        if (getServer().getPluginManager().isPluginEnabled("LuckPerms")) {
            Logger.info("LuckPerms found. Initiating hook...");
        }


        // if (getServer().getPluginManager().isPluginEnabled("MoonClaims")) {
        //     Logger.info("MoonClaims found. Initiating hook...");
        // }
        
        PlayerListener plL = new PlayerListener();
        getServer().getPluginManager().registerEvents(plL, this);
        getServer().getPluginManager().registerEvents(new PurpurListener(this), this);

        getCommand("wildcore").setExecutor(new CmdMoonCore());


        getServer().getScheduler().scheduleSyncRepeatingTask(this, () -> {
            DateFormat dateFormat = new SimpleDateFormat("HH:mm");
            Date date = new Date();

            String firstRestart = getConfig().getString("server.utils.restart.firstRestart");
            String secondRestart = getConfig().getString("server.utils.restart.secondRestart");

            if (dateFormat.format(date).equals(firstRestart)
                    || dateFormat.format(date).equals(secondRestart))
            {

                getServer().getScheduler().scheduleSyncRepeatingTask(this, () -> {
                    if (i == 600) {
                        Lang.sendToAllPlayers(Lang.SERVER_RESTART_10_MINUTES);
                    }
                    if (i == 300) {
                        Lang.sendToAllPlayers(Lang.SERVER_RESTART_5_MINUTES);
                    }
                    if (i == 60) {
                        Lang.sendToAllPlayers(Lang.SERVER_RESTART_1_MINUTE);
                    }
                    if (i == 30) {
                        Lang.sendToAllPlayers(Lang.SERVER_RESTART_30_SECONDS);
                    }
                    if (i == 10) {
                        Lang.sendToAllPlayers(Lang.SERVER_RESTART_10_SECONDS);
                    }
                    if (i == 5) {
                        Lang.sendToAllPlayers(Lang.SERVER_RESTART_5_SECONDS);
                    }
                    if (i == 0) {
                        Lang.sendToAllPlayers(Lang.SERVER_RESTART_FINAL);
                        Lang.sendToAllProxyPlayers(Lang.SERVER_RESTART_FINAL);
                        getBot().sendMessageToDiscord(Lang.SERVER_RESTART_FINAL);
                        i = 600;
                        Bukkit.dispatchCommand(console, command);
                    }
                    i --;
                }, 0L, 20L);
            }

        }, 0, 1200);

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        consoleAppender.worker.interrupt();
        bot.disconnect();
        this.getServer().getMessenger().unregisterOutgoingPluginChannel(this);
        this.getServer().getMessenger().unregisterIncomingPluginChannel(this);

        Logger.info(getName() + " disabled!");
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
      if (!channel.equals("BungeeCord")) {
        return;
      }
      ByteArrayDataInput in = ByteStreams.newDataInput(message);
      String subchannel = in.readUTF();
      System.out.println("received a msg");
      if (subchannel.equals("mooncore-chat")){
        short len = in.readShort();
        byte[] msgbytes = new byte[len];
        in.readFully(msgbytes);
        DataInputStream msgin = new DataInputStream(new ByteArrayInputStream(msgbytes));
        try{
            String newMessage = msgin.readUTF(); // Read the data in the same way you wrote it
            Lang.sendToAllPlayers(newMessage, false, false);
        } catch (IOException e){
            e.printStackTrace();
        }
      }

      
    }

    public Bot getBot() {
        return bot;
    }

    public static MoonCore getInstance() {
        return instance;
    }

    public String getPlayerPrefix(Player player){
        User user = luckPerms.getPlayerAdapter(Player.class).getUser(player);
        String prefix = user.getCachedData().getMetaData().getPrefix();
        if (prefix==null) return "";
        return prefix;
    }

    public String getPlayerSuffix(Player player){
        User user = luckPerms.getPlayerAdapter(Player.class).getUser(player);
        String suffix = user.getCachedData().getMetaData().getSuffix();
        if (suffix==null) return "";
        return suffix;

    }

    public String getPrimaryGroup(Player player){
        User user = luckPerms.getPlayerAdapter(Player.class).getUser(player);
        String parent = user.getPrimaryGroup();
        String displayName = luckPerms.getGroupManager().getGroup(parent).getDisplayName();
        if (displayName!=null) return displayName;
        if (parent.equals("default")) return Config.DEFAULT_RANK;
        return parent;
    }
}