plugins {
  `java-library`
  id("io.papermc.paperweight.userdev") version "1.3.7"
  id("com.github.johnrengelman.shadow") version "7.1.2"
}

group = "moonlit.alexis.mooncore"
version = "1.0"
description = "A private plugin used on the Moonlit Den Minecraft server"

java {
  toolchain.languageVersion.set(JavaLanguageVersion.of(17))
}

dependencies {
  paperweightDevBundle("org.purpurmc.purpur", "1.19-R0.1-SNAPSHOT")
  compileOnly("org.purpurmc.purpur", "purpur-api", "1.19-R0.1-SNAPSHOT")
  api("net.dv8tion:JDA:4.3.0_310"){
    exclude(group = "club.minnced", module = "opus-java")
  }
  implementation("club.minnced:discord-webhooks:0.7.5")
  compileOnly("net.luckperms:api:5.4")
  implementation("com.vdurmont:emoji-java:4.0.0")
  implementation("org.apache.logging.log4j:log4j-core:2.13.3")
  implementation("org.apache-extras.beanshell:bsh:2.0b6")
}

repositories {
  mavenCentral()
  maven("https://repo.purpurmc.org/snapshots")
  maven("https://nexus.hc.to/content/repositories/pub_releases")
  maven("https://repo1.maven.org/maven2/net/luckperms/api/")
  maven("https://mavenrepo.cubekrowd.net/artifactory/repo/")
  maven("https://m2.dv8tion.net/releases")
  maven("https://jitpack.io")
}

tasks {
  assemble {
    dependsOn(reobfJar)
  }

  compileJava {
    options.encoding = Charsets.UTF_8.name()
    options.release.set(17)
  }
}
